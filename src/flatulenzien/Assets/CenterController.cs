using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CenterController : MonoBehaviour, IPointerClickHandler
{
    public GameObject player;
    public GameObject canvas;
    public GameManager gameManager;

    public GameObject TeamBox;
    public GameObject AvailableBox;
    public GameObject CardsBox;

    public List<GameObject> TeamUI;
    public List<GameObject> AvailableUI;

    public GameObject cardInfo;

    public GameObject monPrefab;
    public GameObject cardPrefab;
    private PlayerTeam playerT;

    private GameObject selectedMon = null;


    // Start is called before the first frame update
    void Start()
    {
        playerT = player.GetComponent<PlayerTeam>();
        //TODO remove
        // InitializeCanvas();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OpenCenter()
    {
        playerT.HealTeam();
        canvas.SetActive(true);
        InitializeCanvas();
    }

    public void CloseCenter()
    {
        canvas.SetActive(false);
    }

    private void DrawTeam()
    {
        foreach (Transform child in TeamBox.transform)
        {
            GameObject.Destroy(child.gameObject);
        }

        TeamUI.Clear();
        for (int i = 0; i < 3; i++)
        {
            GameObject tmpMon = Instantiate(monPrefab, TeamBox.transform);
            Debug.Log(playerT);
            if (i < playerT.Team.Team.Count)
            {
                tmpMon.GetComponent<MonButton>().Initialize(playerT.Team.Team[i]);
            }
   
            TeamUI.Add(tmpMon);
        }
    }

    private void DrawAvailable()
    {
        foreach (Transform child in AvailableBox.transform)
        {
            GameObject.Destroy(child.gameObject);
        }

        AvailableUI.Clear();
        foreach (Monster mon in playerT.availableMons)
        {
            GameObject tmpMon = Instantiate(monPrefab, AvailableBox.transform);
            tmpMon.GetComponent<MonButton>().Initialize(mon);
            AvailableUI.Add(tmpMon);
        }
    }

    private void InitializeCanvas()
    {
        playerT = player.GetComponent<PlayerTeam>();

        //Fill Team slots
        DrawTeam();

        //Fill Available slots
        DrawAvailable();
        RenderCards();
    }

    public void OnClickMon(GameObject sourceMonButton)
    {
        if (AvailableUI.Contains(sourceMonButton)){
            selectedMon = sourceMonButton;
        } else {
            if (selectedMon)
            {
                SwitchMon(selectedMon, sourceMonButton);
            }
        }
    }

    private void SwitchMon(GameObject sourceMon, GameObject targetSlot)
    {
        AvailableUI.Remove(sourceMon);
        if (targetSlot.GetComponent<MonButton>().mon != null) {
            AvailableUI.Add(targetSlot);
            targetSlot.transform.SetParent(AvailableBox.transform);
        } else
        {
            Destroy(targetSlot);
        }
        int index = TeamUI.IndexOf(targetSlot);
        TeamUI[index] = sourceMon;
        sourceMon.transform.SetParent(TeamBox.transform);
        selectedMon = null;
        for (int i = 0; i < TeamUI.Count; i++)
        {
            TeamUI[i].transform.SetSiblingIndex(i);
        }
        RenderCards();
    }

    private void RenderCards()
    {
        foreach (Transform child in CardsBox.transform)
        {
            GameObject.Destroy(child.gameObject);
        }

        foreach (GameObject mon in TeamUI)
        {
            Monster monster = mon.GetComponent<MonButton>().mon;
            if (monster)
            {
                foreach (Abilitie abilitie in monster.Abilities)
                {
                    GameObject card = Instantiate(cardPrefab, CardsBox.transform);
                    Debug.Log(abilitie);
                    card.GetComponent<CardUi2>().Initialize(this, abilitie, 0);
                }
            }
        }
    }

    public void OpenCard(GameObject card)
    {
        cardInfo.GetComponent<DisplayCard>().ShowCard(card.GetComponent<CardUi2>().abilitie);
        cardInfo.SetActive(true);
    }

    public void CloseCard()
    {
        cardInfo.SetActive(false);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        CloseCard();
    }

    public void SaveTeam()
    {
        List<Monster> newTeam = new List<Monster>();
        foreach(GameObject mon in TeamUI)
        {
            Monster monster = mon.GetComponent<MonButton>().mon;
            if (monster != null)
                newTeam.Add(monster);
        }
        playerT.ChangeTeam(newTeam);
        playerT.availableMons.Clear();
        foreach (GameObject mon in AvailableUI)
        {
            Monster monster = mon.GetComponent<MonButton>().mon;
            if (monster != null)
                playerT.availableMons.Add(monster);
        }
        gameManager.CloseCenter();
    }

    /*
     * TODO
     * Order Team correctly
     * Each Button gets an Index for return value (Class MonButton)
     * Click right Select m on
     * click left interakt with select mon else nothing
     * card array show cards of selected mon
     * hover over card show detail preview
     */
}
