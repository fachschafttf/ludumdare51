using System;
using System.Collections.Generic;

[Serializable]
public class CardEffect // : MonoBehaviour
{
    public Abilitie.Target Target;
    public Abilitie.InteractionType InteractionType;
    public int Value;
}
