using System;
using UnityEngine;

// ReSharper disable once CheckNamespace
public class AudioController : MonoBehaviour
{
    public static AudioController Instance;

    [Range(0, 10)]
    public int MusicVolume;
    [Range(0, 10)]
    public int SfxVolume;

    private Sound _currentMusicTrack;

    public Sound[] MusicTracks;
    public Sound[] SfxSounds;

    // ReSharper disable once UnusedMember.Local
    void Awake()
    {
        DontDestroyOnLoad(gameObject);

        // Keep existing controller from different scene.
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            foreach (var sound in SfxSounds)
            {
                try
                {
                    sound.Stop();
                }
                catch
                {
                    // ignored
                }
            }
            Destroy(gameObject);
            return;
        }

        MusicVolume = PlayerPrefs.GetInt("musicVolume", 1);
        SfxVolume = PlayerPrefs.GetInt("sfxVolume", 1);
        LoadSounds();
    }

    void Start() {
        
        PlayMusic(MusicTracks[0].Clip.name);
    }


    private void LoadSounds()
    {
        foreach (var sound in SfxSounds)
        {
            sound.Source = gameObject.AddComponent<AudioSource>();
            sound.Source.clip = sound.Clip;
            sound.Source.volume = sound.Volume;
            sound.Source.loop = sound.Loop;
        }

        foreach (var sound in MusicTracks)
        {
            sound.Source = gameObject.AddComponent<AudioSource>();
            sound.Source.clip = sound.Clip;
            sound.Source.volume = sound.Volume;
            sound.Source.loop = sound.Loop;
        }
    }

    public void PlayMusic(string soundName)
    {
        var sound = Array.Find(MusicTracks, s => s.Clip.name == soundName);
        Play(sound, (float)MusicVolume / 10, soundName);
        if (sound != null)
        {
            _currentMusicTrack?.Source.Stop();
            _currentMusicTrack = sound;
        }
    }

    public void SetSoundVolume(string soundName, float volume)
    {
        var sound = GetSound(soundName);
        sound.SetMasterVolume(SfxVolume * volume);
    }

    private Sound GetSound(string soundName)
    {
        return Array.Find(SfxSounds, s => s.Clip.name == soundName);
    }

    public void PlaySound(string soundName)
    {
        PlaySoundWithModifiedVolume(soundName, 1f);
    }

    public void PlaySoundRelativeToDistance(string soundName, float Distance) 
    {
            var volume = Mathf.Min(0.2f/Distance - 0.1f, 1);
            if (volume > 0){
                AudioController.Instance?.PlaySoundWithModifiedVolume(soundName, volume);
            }
    }
    public void PlaySoundWithModifiedVolume(string soundName, float volume = 1f)
    {
        var sound = GetSound(soundName);
        Play(sound, volume * SfxVolume / 10, soundName, true);
    }

    public void StopSound(string soundName)
    {
        var sound = GetSound(soundName);
        sound?.Stop();
    }

    private void Play(Sound sound, float volume, string soundName, bool oneShot = false)
    {

        if (sound != null)
        {
            if (oneShot) {
                sound.PlayOneShot(volume);
            }
            else {                
                sound.Play(volume);
            }
        }
        else
        {
            Debug.LogError("Clip " + soundName + " not found for playback");
        }
    }

    public void PlaySoundRelativeToCameraPosition(string soundName, Vector3 soundOriginPosition)
    {
        var cameraController = FindObjectOfType<Camera>();
        if (!cameraController)
        {
            Debug.LogError("No Camera Controller found");
            return;
        }
        var distanceFromCamera = Vector3.Distance(cameraController.transform.position, soundOriginPosition);

        var farDistance = 4.8f;
        var closeDistance = 4.2f;


        var distanceSfxVolumeFactor = 1 - Mathf.Max(0, Mathf.Min((distanceFromCamera - closeDistance) / (farDistance - closeDistance), 1));

        // Sound Loudness with dezibel stuff
        var modifiedVolume = Mathf.Pow(Mathf.Clamp01(distanceSfxVolumeFactor), Mathf.Log(10, 4));

        PlaySoundWithModifiedVolume(soundName, modifiedVolume);
    }

    public void StopAllSfxSounds()
    {
        foreach (var sound in SfxSounds)
        {
            sound.Stop();
        }
    }

    public void ChangeMusicVolume(int change)
    {
        MusicVolume += change;
        EnsureMusicIntervals();
        _currentMusicTrack.SetMasterVolume((float)MusicVolume / 10);
        PlayerPrefs.SetInt("musicVolume", MusicVolume);
    }

    public void ChangeSfxVolume(int change)
    {
        SfxVolume += change;
        EnsureMusicIntervals();
        foreach (var sound in SfxSounds)
        {
            sound.SetMasterVolume((float)SfxVolume / 10);
        }
        PlayerPrefs.SetInt("sfxVolume", SfxVolume);
    }

    public void SetMusicVolume(int change)
    {
        MusicVolume = change;
        EnsureMusicIntervals();
        _currentMusicTrack.SetMasterVolume((float)MusicVolume / 10);
    }

    public void SetSfxVolume(int change)
    {
        SfxVolume = change;
        EnsureMusicIntervals();
        foreach (var sound in SfxSounds)
        {
            sound.SetMasterVolume((float)SfxVolume / 10);
        }
    }

    private void EnsureMusicIntervals()
    {
        int minVolume = 0;
        int maxVolume = 10;

        MusicVolume = Mathf.Min(maxVolume, Mathf.Max(minVolume, MusicVolume));
        SfxVolume = Mathf.Min(maxVolume, Mathf.Max(minVolume, SfxVolume));
        PlayerPrefs.SetInt("musicVolume", MusicVolume);
        PlayerPrefs.SetInt("sfxVolume", SfxVolume);
    }
}
