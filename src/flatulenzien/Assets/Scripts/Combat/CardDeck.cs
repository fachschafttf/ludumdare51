using System.Collections.Generic;
using UnityEngine;

public class CardDeck
{
    public List<CombatMonster> monTeam;
    public List<MonDeck> decks;
    public List<Abilitie> hand;

    public CardDeck(List<CombatMonster> _monTeam)
    {
        monTeam = _monTeam;
        decks = new List<MonDeck>();

        foreach(CombatMonster mon in monTeam)
        {
            decks.Add(new MonDeck(mon.MonsterInformation));
        }
    }

    //draw Hand for all Mons
    public List<Abilitie> DrawHand()
    {
        hand = new List<Abilitie>();
        foreach(MonDeck md in decks)
        {
            hand.AddRange(md.drawHand());
        }
        return hand;
    }

    //discard Hand for all mons
    public void EndTurn()
    {
        foreach (MonDeck md in decks)
        {
            md.EndTurn();
        }
        hand = new List<Abilitie>();
    }

    public void RemoveMon(int indx)
    {
        decks.RemoveAt(indx);
    }
}

public class MonDeck
{
    public List<Abilitie> all;
    public List<Abilitie> drawable;
    public List<Abilitie> trash;
    public List<Abilitie> hand;
    public Monster mon;
    
    //start situation for deck all cards in drawable, shuffled
    public MonDeck(Monster _mon)
    {

        // all = _mon.Abilities;
        all = new List<Abilitie>(_mon.Abilities);
        mon = _mon;

        drawable = new List<Abilitie>(all);

        drawable.Shuffle();
        trash = new List<Abilitie>();
        hand = new List<Abilitie>();

    }

    //draw two cards to hand from drawable, return hand
    public List<Abilitie> drawHand()
    {
        for(int i = 0; i < 2; i++)
        {
            if(drawable.Count <= 0)
            {
                ReShuffle();
            }
            Abilitie tmp = drawable[0];
            hand.Add(tmp);
            drawable.RemoveAt(0);
        }
        return hand;
    }

    //discard hand to trash
    public void EndTurn()
    {
        // Debug.Log("a:" + trash.Count);
        foreach(Abilitie a in hand)
        {
            trash.Add(a);
            // Debug.Log("b:" + trash.Count);
        }
        hand = new List<Abilitie>();
        // Debug.Log("c:" + trash.Count);
    }

    private void ReShuffle()
    {
        if (trash.Count == 0)
        {
            Debug.Log("a�lkshdflkasjfdlkasdnf1111!!!!!!!!!!");
        }
        trash.Shuffle();
        if(trash.Count == 0)
        {
            Debug.Log("a�lkshdflkasjfdlkasdnf");
        }
        drawable = new List<Abilitie>(trash);
        trash = new List<Abilitie>();
    }
}

public static class ExtensionMethods
{
    //Even though they are used like normal methods, extension
    //methods must be declared static. Notice that the first
    //parameter has the 'this' keyword followed by a List
    //variable. This variable denotes which class the extension
    //method becomes a part of.

    public static void Shuffle(this List<Abilitie> abi)
    {
        for (int i = 0; i < abi.Count; i++)
        {
            Abilitie tmp = abi[i];
            int rnd = Random.Range(i, abi.Count);
            abi[i] = abi[rnd];
            abi[rnd] = tmp;
        }
    }
}

