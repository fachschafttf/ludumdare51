using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// this class should hold the CURRENT values for monsters
/// </summary>
[Serializable]
public class CombatMonster
{
    public int CurrentHealth;
    public int MaxHealth;
    public Monster MonsterInformation;
    public bool hasDied;

    /// <summary>
    /// Constructor to create an usable monster from the scriptableObject
    /// </summary>
    public CombatMonster(Monster monster)
    {
        MonsterInformation = monster;
        MaxHealth = monster.Health;
        CurrentHealth = MaxHealth;
        hasDied = false;
    }

    public float GetPercentHP()
    {
        return CurrentHealth / (float)MaxHealth;
    }

    /// <summary>
    /// returns true if the monster died
    /// </summary>
    /// <param name="type"></param>
    /// <param name="value"></param>
    /// <returns></returns>
    public bool InteractHP(Abilitie.InteractionType type, int value)
    {
        switch (type)
        {
            case Abilitie.InteractionType.Damage:
                ApplyDamage(value);
                return CheckDeath();
            case Abilitie.InteractionType.Heal:
                ApplyHeal(value);
                break;
            default:
                break;
        }
        return false;
    }

    /// <summary>
    /// lower clamp hp to 0
    /// </summary>
    /// <param name="value"></param>
    private void ApplyDamage(int value)
    {
        CurrentHealth = Math.Max(0, CurrentHealth - value);
    }

    private bool CheckDeath()
    {
        // here we could add something like last stand on 1 hp...
        return CurrentHealth <= 0;
    }

    /// <summary>
    /// upper clamp hp to max value
    /// </summary>
    /// <param name="value"></param>
    private void ApplyHeal(int value)
    {
        CurrentHealth = Math.Min(CurrentHealth + value, MaxHealth);
    }

    internal void ShouldDie()
    {
        hasDied = true;
    }
}
