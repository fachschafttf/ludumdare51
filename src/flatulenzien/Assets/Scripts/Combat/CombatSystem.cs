using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public enum BattleState { START, DRAW, PLAYERTURN, COMBAT, WON, LOST }
public class CombatSystem : MonoBehaviour
{
    public BattleState state;

    public GameObject canvas;
    public GameManager gameManager;

    public List<GameObject> playerBattleStations;
    public List<GameObject> enemyBattleStations;

    public List<GameObject> _playerBattleStations;
    public List<GameObject> _enemyBattleStations;


    public GameObject player;
    private EnemyTeam enemy;

    public CardDeck playerDeck;
    public CardDeck enemyDeck;

    public List<CombatMonster> playerCombatMonster;
    public List<CombatMonster> enemyCombatMonster;

    public Timeline timeline;

    public Sprite[] backgrounds;
    public GameObject backgroundCanvas;

    public List<Abilitie> playerHand;
    public bool[] cardPlayed;
    public List<Abilitie> enemyHand;
    public bool[] enemyCardPlayed;

    // Start is called before the first frame update
    void Start()
    {
        state = BattleState.START;
        timeline = new Timeline();
        _enemyBattleStations = new List<GameObject>(enemyBattleStations);
        _playerBattleStations = new List<GameObject>(playerBattleStations);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetupBattle(EnemyTeam _enemy)
    {
        enemyBattleStations = new List<GameObject>(_enemyBattleStations);
        playerBattleStations = new List<GameObject>(_playerBattleStations);
        enemy = _enemy;
        playerCombatMonster = new List<CombatMonster>();
        enemyCombatMonster = new List<CombatMonster>();

        canvas.SetActive(true);
        LoadMONs();
        LoadDecks();
        //beginn with drawstep
        state = BattleState.DRAW;
        Draw();
        SetBackground();
    }

    /*
     * Event Funktions
     * Called From UI
     */

    public void OnEndTurnButton()
    {
        state = BattleState.COMBAT;
        //solve turn
        IEnumerator combat = CombatPhase();

        canvas.GetComponent<BattleUI>().StartProgressbar();
        StartCoroutine(combat);
        // CombatPhase();
    }

    private void AfterCombat()
    {
        //check for player lost
        //check for player won
        // CheckGameEndConditions();
        CheckDeathStates();

        //If Combat still going Do Next Turn
        if (state == BattleState.COMBAT)
        {
            EndTurn();
            state = BattleState.DRAW;
            Draw();
        }
    }

    public void OnAddCardToTimeline(int cardIndx, int startTime)
    {
        int monsterIndx = cardIndx / 2;
        timeline.AddAbilitie(timeline.PlayerElements, playerHand[cardIndx], startTime, playerCombatMonster[0]);
    }

    public int OnUndo()
    {
        return timeline.UndoNewestAddition(timeline.PlayerElements);
    }

    public bool DeleteFromTimeline(int startPoint)
    {
        return timeline.DeletePlayerAbilityStartingAt(startPoint);
    }

    public bool CanPLaceCard(int startTime, int cardIndex)
    {
        return Timeline.IsInsertPossible(timeline.PlayerElements, startTime, playerHand[cardIndex]);
    }

    public List<Tuple<int, int>> EnemyTimeline()
    {
        return timeline.GetEnemyTimeIntervals();
    }

    /*
     * Internal Funktions start here
     * Hopefully nobody has to look at this :D
     * 
     */

    private void EndTurn()
    {
        //Handle End Turn Event (clear up everything before Next Turn)
        playerDeck.EndTurn();
        enemyDeck.EndTurn();
        timeline.ResetTimeline();
        //Clear Hand
        playerHand = new List<Abilitie>();
        cardPlayed = new bool[0];
        enemyHand = new List<Abilitie>();
        enemyCardPlayed = new bool[0];
    }
    private void SetBackground()
    {
        int indx = UnityEngine.Random.Range(0, backgrounds.Length);
        backgroundCanvas = canvas.transform.Find("Background").gameObject;
        backgroundCanvas.GetComponent<Image>().sprite = backgrounds[indx];
    }

    void Draw()
    {
        // Draw cards
        playerHand = playerDeck.DrawHand();
        cardPlayed = new bool[playerHand.Count];
        enemyHand = enemyDeck.DrawHand();
        enemyCardPlayed = new bool[enemyHand.Count];

        EnemyTurn();

        state = BattleState.PLAYERTURN;
        //TODO Change UI with new Cards
        canvas.GetComponent<BattleUI>().StartRound();

        // TODO match playerhand to monster that is casting the spell
        //timeline.AddAbilitie(timeline.PlayerElements, playerHand[0], 2, playerCombatMonster[0]);
        //timeline.AddAbilitie(timeline.EnemyElements, enemyHand[0], 3, playerCombatMonster[0]);
        //timeline.AddAbilitie(timeline.EnemyElements, enemyHand[0], 9, playerCombatMonster[0]);

        // testing now, remove this later
        //OnEndTurnButton();
    }

    private void EnemyTurn()
    {
        // smart^{TM} decisions
        var indicesOfCardsToPlay = EnemyAI.ChooseCardsToPlay(timeline, enemyHand);
        
        int currentTime = 0;
        for (int i = 0; i < indicesOfCardsToPlay.Count; i++)
        {
            // current ability
            Abilitie currentAbility = enemyHand[i];

            // current monster
            int monsterIndx = i / 2;
            CombatMonster currentMonster = enemyCombatMonster[monsterIndx];

            timeline.AddAbilitie(timeline.EnemyElements, enemyHand[i], currentTime, currentMonster);

            // next start time is += duration of this one
            currentTime += currentAbility.CastTime;
        }
        //Debug.Log($"enemy has planned {timeline.EnemyElements.Count} moves: \n");
        foreach (var item in timeline.EnemyElements)
        {
            //Debug.Log($"move: {item.Abilitie.AbilitieName} at {item.StartTime}");
        }
    }

    private void CheckDeathStates()
    {
        DeleteDeadMons();
    }

    private void DeleteDeadMons()
    {
        List<int> toDeleteP = new List<int>();
        List<int> toDeleteE = new List<int>();
        for (int i = 0; i < playerCombatMonster.Count; i++)
        {
            if (playerCombatMonster[i].hasDied)
            {
                toDeleteP.Add(i);
            }
        }
        DeleteDeadPlayerMons(toDeleteP);
        //check if LOST
        if(playerCombatMonster.Count <= 0)
        {
            GameOver();
            return;
        }
        for (int i = 0; i < enemyCombatMonster.Count; i++)
        {
            if (enemyCombatMonster[i].hasDied)
            {
                toDeleteE.Add(i);
            }
        }
        DeleteDeadEnemyMons(toDeleteE);
        if(enemyCombatMonster.Count <= 0)
        {
            CombatWON();
        }
    }

    private void GameOver()
    {
        EndTurn();
        canvas.SetActive(false);
        state = BattleState.LOST;
        Debug.Log("YOU DIED!");
        gameManager.FinishedFight(state);
    }

    private void CombatWON()
    {
        EndTurn();
        canvas.SetActive(false);
        state = BattleState.WON;
        Debug.Log("YOU WON :)");
        gameManager.FinishedFight(state);
    }

    private void DeleteDeadPlayerMons(List<int> toDelete)
    {
        for(int i = 0; i<toDelete.Count; i++)
        {
            //TODO Death Animations?
            playerCombatMonster.RemoveAt(toDelete[i] - i);
            playerBattleStations.RemoveAt(toDelete[i] - i);
            playerDeck.RemoveMon(toDelete[i] - i);
        }
    }

    private void DeleteDeadEnemyMons(List<int> toDelete)
    {
        for (int i = 0; i < toDelete.Count; i++)
        {
            enemyCombatMonster.RemoveAt(toDelete[i] - i);
            enemyBattleStations.RemoveAt(toDelete[i] - i);
            enemyDeck.RemoveMon(toDelete[i] - i);
        }
    }

    private IEnumerator CombatPhase()
    {
        // we have a tilenine with abilities that should happen,
        // probably really important to take exactly 10 seconds now!
        // atleast it should not be instant!

        // maybe use timeline to play actions?

        var combat = timeline.Combat(playerCombatMonster, enemyCombatMonster);

        for (int i = 0; i < 10; i++)
        {
            // Debug.Log($"combat phase {i}");
            combat.MoveNext();
            yield return new WaitForSeconds(1f);
        }

        AfterCombat();
    }

    void LoadMONs()
    {
        Debug.Log(player.GetComponent<PlayerTeam>().teamStats.Count);
        for(int i = 0; i<3; i++)
        {
            if (i >= enemy.Team.Team.Count)
            {
                enemyBattleStations[i].SetActive(false);
            }
            else
            {
                enemyBattleStations[i].SetActive(true);
                //set enemy Battlestation Name and Sprite
                enemyBattleStations[i].transform.Find("MON").gameObject.GetComponent<Image>().sprite = enemy.Team.Team[i].Artwork;
                enemyBattleStations[i].transform.Find("MonName").gameObject.GetComponent<TMP_Text>().text = enemy.Team.Team[i].MonsterName;
                enemyCombatMonster.Add(new CombatMonster(enemy.Team.Team[i]));
                enemyBattleStations[i].transform.Find("HP").gameObject.GetComponent<HealthBar>().mon = enemyCombatMonster[enemyCombatMonster.Count - 1];
            }
           
            if (i >= player.GetComponent<PlayerTeam>().teamStats.Count)
            {
                playerBattleStations[i].SetActive(false);
            }
            else
            {
                playerBattleStations[i].SetActive(true);
                //set player Battlestation Name and Sprite
                playerBattleStations[i].transform.Find("MON").gameObject.GetComponent<Image>().sprite = player.GetComponent<PlayerTeam>().teamStats[i].MonsterInformation.Artwork;
                playerBattleStations[i].transform.Find("MonName").gameObject.GetComponent<TMP_Text>().text = player.GetComponent<PlayerTeam>().teamStats[i].MonsterInformation.MonsterName;
                playerCombatMonster.Add(player.GetComponent<PlayerTeam>().teamStats[i]);
                playerBattleStations[i].transform.Find("HP").gameObject.GetComponent<HealthBar>().mon = playerCombatMonster[playerCombatMonster.Count - 1];
            }
        }
    }

    void LoadDecks()
    {
        playerDeck = new CardDeck(playerCombatMonster);
        enemyDeck = new CardDeck(enemyCombatMonster);
    }

    void removeBattleStation(bool player, int indx)
    {
        //move up mon if one is dead
    }

    List<Abilitie> Shuffle(List<Abilitie> _abilities)
    {
        List<Abilitie> tmp = new List<Abilitie>();
        while(_abilities != null)
        {
            Abilitie t = _abilities[UnityEngine.Random.Range(0, _abilities.Count - 1)];
            tmp.Add(t);
            _abilities.Remove(t);
        }
        return tmp;
    }
}
