using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DisplayCard : MonoBehaviour
{
    [Header("Data Container asset here")]
    public Abilitie Card;

    [Header("Children of Gameobject here")]
    public Image Image;
    public TextMeshProUGUI NameText;
    public TextMeshProUGUI CastTimeText;
    public TextMeshProUGUI DescriptionText;


    // Start is called before the first frame update
    void Start()
    {
        InitCardWithData();
    }

    public void InitCardWithData()
    {
        NameText.text = Card.AbilitieName;
        DescriptionText.text = Card.Description;
        CastTimeText.text = Card.CastTime.ToString();
        Image.sprite = Card.Artwork;
    }

    public void ShowCard(Abilitie card)
    {
        Card = card;
        InitCardWithData();
    }
}
