using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class EnemyAI
{
    public static List<int> ChooseCardsToPlay(Timeline timeline, List<Abilitie> hand, float chanceToPreferRandom = 0.5f)
    {
        // Represent The cards by  a list of tuples [(castTime0, 0), (castTime1, 1), (castTime2, 2), ... (castTime5, 5)]
        // we only care for the duration it takes (and save the index to return the correct cards later on)
        // further sort the sort items by their CastTime
        var durationAtIndex = new List<Tuple<int, int>>();
        for (int i = 0; i < hand.Count; i++)
        {
            durationAtIndex.Add(new Tuple<int, int>(hand[i].CastTime, i));
        }

        // Randomly decide for a method to choose the cards
        var rng = new System.Random();
        float rand = rng.Next();
        List<Tuple<int, int>> wanted = chanceToPreferRandom > rand ? GreedyChooseSet(durationAtIndex) : RandomChooseSet(durationAtIndex);

        // reduce the tuples to their indices (2nd value) since duration  is irrelevant
        List<int> ChosenCards = new List<int>();
        for (int i = 0; i < wanted.Count; i++)
        {
            ChosenCards.Add(wanted[i].Item2);
        }

        return ChosenCards;

        // Chosen are the indices of the cards we want to play.
        // I dont know if we need those? or if we need the actual abilites
        //return actions;
    }

    private static List<Tuple<int, int>> GreedyChooseSet(List<Tuple<int, int>> durationAtIndex, int timeLimit = 10)
    {
        // Sort descending by time to cast
        durationAtIndex.OrderByDescending(o => o.Item1).ToList();

        var resultList = new List<Tuple<int, int>>();
        int remainingTime = timeLimit;

        for (int i = 0; i < durationAtIndex.Count; i++)
        {
            int currentCost = durationAtIndex[i].Item1;
            if (remainingTime >= currentCost)
            {
                remainingTime -= currentCost;
                resultList.Add(durationAtIndex[i]);
            }
        }
        return resultList;
    }

    private static List<Tuple<int, int>> RandomChooseSet(List<Tuple<int, int>> durationAtIndex, int timeLimit = 10)
    {
        var resultList = new List<Tuple<int, int>>();
        int remainingTime = timeLimit;

        // randomly sort the list, we still iterate over and take first come first serve
        var rng = new System.Random();
        durationAtIndex.OrderBy(a => rng.Next()).ToList();

        for (int i = 0; i < durationAtIndex.Count; i++)
        {
            int currentCost = durationAtIndex[i].Item1;
            if (remainingTime >= currentCost)
            {
                remainingTime -= currentCost;
                resultList.Add(durationAtIndex[i]);
            }
        }
        return resultList;
    }
}
