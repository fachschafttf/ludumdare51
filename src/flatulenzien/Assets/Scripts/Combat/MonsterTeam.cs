using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class MonsterTeam
{
    [SerializeField]
    public List<Monster> Team;

    public MonsterTeam(List<Monster> team)
    {
        this.Team = new List<Monster>(team);
    }

    public int TeamCount()
    {
        return Team.Count;
    }
}
