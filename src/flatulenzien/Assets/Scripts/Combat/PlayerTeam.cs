using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTeam : MonoBehaviour
{
    public MonsterTeam Team;
    public List<CombatMonster> teamStats;

    public List<Monster> availableMons;

    // Start is called before the first frame update
    void Start()
    {
        GenerateCombatTeamFromTeam();
    }

    private void GenerateCombatTeamFromTeam()
    {
        teamStats = new List<CombatMonster>();
        foreach (Monster mon in Team.Team)
        {
            teamStats.Add(new CombatMonster(mon));
        }
    }

    public void ChangeTeam(List<Monster> team)
    {
        SetCurrentTeamOnBench();
        BuildNewTeam(team);
        //after Monster Team is changes Update Combat Stats
        GenerateCombatTeamFromTeam();
    }

    private void BuildNewTeam(List<Monster> team)
    {
        Team = new MonsterTeam(team);
    }

    private void SetCurrentTeamOnBench()
    {
        // (with checking for duplicated monster) move all from Team to availableMons
        for (int i = 0; i < Team.Team.Count; i++)
        {
            if (! availableMons.Contains(Team.Team[i]))
            {
                availableMons.Add(Team.Team[i]);
            }
        }
    }

    /// <summary>
    /// swaps monster from bench to current team.
    /// returns if adding was successful
    /// </summary>
    /// <returns></returns>
    public bool AddMon(Monster mon)
    {
        if (Team.TeamCount() < 3)
        {
            Team.Team.Add(mon);
            teamStats.Add(new CombatMonster(mon));
            availableMons.Remove(mon);
            return true;
        }
        
        return false;
    }

    /// <summary>
    /// return whether removing was successful
    /// </summary>
    /// <returns></returns>
    public bool RemoveMon()
    {
        int size = Team.TeamCount();
        if (size <= 1)
        {
            Debug.Log("team size cant be 0");
            return false;
        }

        // bench monster
        Monster mon = Team.Team[size - 1];
        availableMons.Add(mon);
        return true;
    }

    public void HealTeam()
    {
        foreach(CombatMonster mon in teamStats)
        {
            mon.CurrentHealth = mon.MaxHealth;
        }
    }

    public void AddNewMonsterToAvailableMonster(Monster monster)
    {
        if (!availableMons.Contains(monster))
        {
            availableMons.Add(monster);
        }
    }
}
