using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class Timeline
{
    [Serializable]
    public class TimelineElement
    {
        public int StartTime;
        public Abilitie Abilitie;
        public CombatMonster CastingMonster;

        public TimelineElement(int startTime, Abilitie abilitie, CombatMonster castingMonster)
        {
            StartTime = startTime;
            Abilitie = abilitie;
            CastingMonster = castingMonster;
        }
    }

    // two timelines, one for each player

    public List<TimelineElement> PlayerElements;
    public List<TimelineElement> EnemyElements;

    public Timeline()
    {
        ResetTimeline();
    }

    public static bool IsInsertPossible(List<TimelineElement> timeline, int startTime, Abilitie abilitie)
    {
        // get end time
        int endTime = startTime + abilitie.CastTime;

        // without further constraints:
        // if the "arrow" does not fit in the timeline, we know that it doesnt fit
        // 1st    2nd    3rd    4th    5th    6th    7th    8th    9th    10th
        // [0,1]  [1,2]  [2,3]  [3,4]  [4,5]  [5,6]  [6,7]  [7,8]  [8,9]  [9,10] 
        //  ^left is start time,   ^right is end time
        if (startTime < 0 || endTime > 10)
        {
            return false;
        }

        // we need empty space on every time step from start to end
        // there must not be any ability that lands inbetween the space.
        foreach (var otherA in timeline)
        {
            if (Overlaps(otherA, startTime, endTime))
            {
                return false;
            }
        }

        return true;
    }

    /// <summary>
    /// check whether two events overlap
    /// </summary>
    /// <param name="otherA"></param>
    /// <param name="thisStart"></param>
    /// <param name="thisEnd"></param>
    /// <returns></returns>
    public static bool Overlaps(TimelineElement otherA, int thisStart, int thisEnd)
    {
        int otherStart = otherA.StartTime;
        int otherEnd = otherStart + otherA.Abilitie.CastTime - 1;

        // many different cases, i hope i cover all
        if (otherStart < thisStart)
        {
            // other starts first: check if we are in (a) or (b)
            // (a)   is bad (true)   |     (b)   is fine
            // [ other ]             |     [ other ]
            //      [ this ]         |              [ this ]
            if (otherEnd >= thisStart)
            {
                return true;
            }
        }
        else // otherStart >= startTime
        {
            // this starts first: check if we are in (a) or (b)
            // (a)    is bad (true)  |    (b) is fine
            // [ this ]              |    [ this ]
            //      [ other ]        |            [ other ]
            if (otherStart < thisEnd)
            {
                return true;
            }
        }

        return false;
    }

    //IEnumerator GetNextEvent()
    //{

    //}

    /// <summary>
    /// sorts by start time
    /// </summary>
    /// <returns></returns>
    public List<TimelineElement> GetSortedPlayerAbilities()
    {
        return PlayerElements.OrderBy(o => o.StartTime).ToList();
    }

    /// <summary>
    /// sorty by start time
    /// </summary>
    /// <returns></returns>
    public List<TimelineElement> GetSortedEnemyAbilities()
    {
        return EnemyElements.OrderBy(o => o.StartTime).ToList();
    }

    public List<Tuple<int,int>> GetEnemyTimeIntervals()
    {
        return GetSortedTuplesFromTimeline(false);
    }

    public List<Tuple<int,int>> GetSortedTuplesFromTimeline(bool player = false)
    {
        List<Tuple<int,int>> tuples = new List<Tuple<int,int>>();

        List<TimelineElement> sorted = player ? GetSortedPlayerAbilities() : GetSortedEnemyAbilities();

        for (int i = 0; i < sorted.Count; i++)
        {
            var start = sorted[i].StartTime;
            var end = sorted[i].Abilitie.CastTime;
            var tuple = new Tuple<int,int>(start, end);
            tuples.Add(tuple);
        }
        return tuples;
    }


    public IEnumerator Combat(List<CombatMonster> allyList, List<CombatMonster> enemyList)
    {
        var currentAllyList = allyList;
        var currentEnemyList = enemyList;

        List<TimelineElement> sortedP = GetSortedPlayerAbilities();
        List<TimelineElement> sortedE = GetSortedEnemyAbilities();
        for (int i = 0; i < 10; i++)
        {
            // RESOLVING ABILITIES
            // Player Action should resolve
            if (sortedP.Count > 0)
            {
                if (i == sortedP[0].StartTime + sortedP[0].Abilitie.CastTime)
                {
                    var self = sortedP[0].CastingMonster;
                    sortedP[0].Abilitie.ApplyEffects(self, currentAllyList, currentEnemyList);

                    // remove the ability from our todo list
                    sortedP.RemoveAt(0);
                }
            }
            // Enemy Action should resolve
            if (sortedE.Count > 0)
            {
                if (i == sortedE[0].StartTime + sortedE[0].Abilitie.CastTime)
                {
                    // important Question:
                    // does death of an monster cancel animations that are about to resolve?
                    var self = sortedE[0].CastingMonster;
                    sortedE[0].Abilitie.ApplyEffects(self, currentEnemyList, currentAllyList);
                }

                // remove the ability from our todo list
                sortedE.RemoveAt(0);
            }
            

            // STARTING ABILITIES
            // Player Action is starting now
            if (sortedP.Count > 0)
            {
                if (i == sortedP[0].StartTime)
                {
                    sortedP[0].Abilitie.Starting();
                }
            }
            // Enemy Action is starting now
            if (sortedE.Count > 0)
            {
                if (i == sortedE[0].StartTime)
                {
                    sortedE[0].Abilitie.Starting();
                }
            }

            // Debug.Log($"Combaaaaaat in timeline {i}");
            yield return null;
        }
    }

    public void AddAbilitie(List<TimelineElement> elements, Abilitie newAbilitie, int startTime, CombatMonster castingMonster)
    {
        if (IsInsertPossible(elements, startTime, newAbilitie))
        {
            TimelineElement newElement = new TimelineElement(startTime, newAbilitie, castingMonster);
            elements.Add(newElement);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="elements"></param>
    public int UndoNewestAddition(List<TimelineElement> elements)
    {
        if (elements == null)
        {
            return -1; // no sneaky crash if we press undo at bad time
        }

        // (by design) we just have to delete newest entry
        // (TODO: and maybe tell the ability that it is free again?)
        var size = elements.Count;
        TimelineElement element;
        if (size > 0)
        {
            element = elements[size - 1];
            IsFreeAgain(element.Abilitie);
            elements.RemoveAt(size - 1);
            return element.StartTime;
        }
        return -1;
    }

    public bool DeletePlayerAbilityStartingAt(int startToDelete)
    {
        // using flag and iterating backwards -> this fgunction can theoretically handle the deletions of ALL abilities
        // that start at this time (even tho we maybe dont have abilities with cast time 0 ?)
        bool deleted = false;
        for (int i = PlayerElements.Count - 1; i >= 0; i--)
        {
            if (PlayerElements[i].StartTime == startToDelete)
            {
                IsFreeAgain(PlayerElements[i].Abilitie);
                PlayerElements.RemoveAt(i);

                deleted = true;
            }
        }
        return deleted;
    }

    private void IsFreeAgain(Abilitie abilitie)
    {
        // TODO: sound? animation? whatever?
    }

    public void ResetTimeline()
    {
        PlayerElements = new List<TimelineElement>();
        EnemyElements = new List<TimelineElement>();
    }
}
