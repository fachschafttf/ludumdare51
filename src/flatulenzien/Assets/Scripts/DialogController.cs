using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class DialogController : MonoBehaviour
{
    [SerializeField] private GameObject dialogUI;
    [SerializeField] public GameObject gameManager;
    [Header("Text")]
    [SerializeField] private TextMeshProUGUI textName;
    [SerializeField] private TextMeshProUGUI textBox;
    [Header("Buttons")]
    [SerializeField] private Button button01;
    [SerializeField] private TextMeshProUGUI buttonText01;
    [Space]
    [SerializeField] private Button button02;
    [SerializeField] private TextMeshProUGUI buttonText02;
    [Space]
    [SerializeField] private Button button03;
    [SerializeField] private TextMeshProUGUI buttonText03;
    [Space]
    [SerializeField] private Button button04;
    [SerializeField] private TextMeshProUGUI buttonText04;

    private List<Button> buttons = new List<Button>();
    private List<TextMeshProUGUI> buttonsTexts = new List<TextMeshProUGUI>();

    private void Awake()
    {
        ShowDialog(false);
        buttons.Add(button01);
        buttons.Add(button02);
        buttons.Add(button03);
        buttons.Add(button04);

        buttonsTexts.Add(buttonText01);
        buttonsTexts.Add(buttonText02);
        buttonsTexts.Add(buttonText03);
        buttonsTexts.Add(buttonText04);

    }

    public void ShowDialog(bool _show)
    {
        dialogUI.SetActive(_show);
    }

    public void SetText(string _name, string _textBox)
    {
        textName.text = _name;
        //textBox.text = _textBox;
        StopAllCoroutines();
        StartCoroutine(TypeOutText(_textBox));
    }

    public void SetButtons(List<string> _texts, List<UnityAction> _unityActions)
    {
        buttons.ForEach(button => button.gameObject.SetActive(false));

        for (int i = 0; i < _texts.Count; i++)
        {
            buttonsTexts[i].text = _texts[i];
            buttons[i].gameObject.SetActive(true);
            buttons[i].onClick = new Button.ButtonClickedEvent();
            buttons[i].onClick.AddListener(_unityActions[i]);
        }
    }
    IEnumerator TypeOutText(string _text)
    {
        textBox.text = string.Empty;
        foreach (char letter in _text.ToCharArray())
        {
            textBox.text += letter;
            yield return new WaitForFixedUpdate();
        }
    }
}
