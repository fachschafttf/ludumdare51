using UnityEngine;

public class DialogDataReciever : MonoBehaviour
{
    [SerializeField] protected DialogContainerSO dialogContainer;

    public void SetDialog(DialogContainerSO dialog)
    {
        this.dialogContainer = dialog;
    }

    protected BaseNodeData GetNodeByGuid(string _targetNodeGuid)
    {
        return dialogContainer.allNodes.Find(node => node.nodeGuid == _targetNodeGuid);
    }

    protected BaseNodeData GetNodeByNodePort(DialogNodePort _nodePort)
    {
        return dialogContainer.allNodes.Find(node => node.nodeGuid == _nodePort.InputGuid);
    }

    protected BaseNodeData GetNextNode(BaseNodeData _baseNodeData)
    {
        NodeLinkData nodeLinkData = dialogContainer.nodeLinkDatas.Find(edge => edge.baseNodeGuid == _baseNodeData.nodeGuid);
        return GetNodeByGuid(nodeLinkData.targetNodeGuid);
    }

}
