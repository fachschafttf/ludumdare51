using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEngine.UIElements;

public class DialogSaveAndLoad
{
    private List<Edge> edges => graphView.edges.ToList();
    private List<BaseNode> nodes => graphView.nodes.ToList().Where(node => node is BaseNode).Cast<BaseNode>().ToList();
    private DialogGraphView graphView;
    public DialogSaveAndLoad(DialogGraphView _graphView)
    {
        graphView = _graphView;

    }
    public void Save(DialogContainerSO _dialogContainserSO)
    {
        SaveEdges(_dialogContainserSO);
        SaveNodes(_dialogContainserSO);

        EditorUtility.SetDirty(_dialogContainserSO);
        AssetDatabase.SaveAssets();
    }
    public void Load(DialogContainerSO _dialogContainserSO)
    {
        ClearGraph();
        GenerateNodes(_dialogContainserSO);
        ConnectNodes(_dialogContainserSO);

    }
    #region SAve Stuff
    private void SaveEdges(DialogContainerSO _dialogContainserSO)
    {
        _dialogContainserSO.nodeLinkDatas.Clear();

        Edge[] connectedEdges = edges.Where(edge => edge.input.node != null).ToArray();
        for (int i = 0; i < connectedEdges.Count(); i++)
        {
            BaseNode outputNode = connectedEdges[i].output.node as BaseNode;
            BaseNode inputNode = connectedEdges[i].input.node as BaseNode;

            _dialogContainserSO.nodeLinkDatas.Add(new NodeLinkData
            {

                baseNodeGuid = outputNode.NodeGuid,
                targetNodeGuid = inputNode.NodeGuid
            });
        }
    }

    private void SaveNodes(DialogContainerSO _dialogContainserSO)
    {
        _dialogContainserSO.dialogNodeDatas.Clear();
        _dialogContainserSO.endNodeDatas.Clear();
        _dialogContainserSO.startNodeDatas.Clear();

        foreach (BaseNode node in nodes)
        {
            switch (node)
            {
                case DialogNode dialogNode:
                    _dialogContainserSO.dialogNodeDatas.Add(SaveNodeData(dialogNode));
                    break;
                case StartNode startNode:
                    _dialogContainserSO.startNodeDatas.Add(SaveNodeData(startNode));
                    break;
                case EndNode endNode:
                    _dialogContainserSO.endNodeDatas.Add(SaveNodeData(endNode));
                    break;
                default:
                    break;
            }
        }
    }

    private DialogNodeData SaveNodeData(DialogNode _node)
    {
        DialogNodeData dialogNodeData = new DialogNodeData
        {
            nodeGuid = _node.NodeGuid,
            position = _node.GetPosition().position,
            Text = _node.Text,
            Name = _node.SpeakerName,
            dialogNodePorts = _node.DialogNodePorts
        };

        foreach (DialogNodePort nodePort in dialogNodeData.dialogNodePorts)
        {
            nodePort.OutputGuid = string.Empty;
            nodePort.InputGuid = string.Empty;
            foreach (Edge edge in edges)
            {
                if (edge.output == nodePort.MyPort)
                {
                    nodePort.OutputGuid = (edge.output.node as BaseNode).NodeGuid;
                    nodePort.InputGuid = (edge.input.node as BaseNode).NodeGuid;
                }
            }
        }

        return dialogNodeData;
    }

    private StartNodeData SaveNodeData(StartNode _node)
    {
        StartNodeData nodedata = new StartNodeData()
        {
            nodeGuid = _node.NodeGuid,
            position = _node.GetPosition().position,
        };
        return nodedata;
    }

    private EndNodeData SaveNodeData(EndNode _node)
    {
        EndNodeData nodeData = new EndNodeData()
        {
            nodeGuid = _node.NodeGuid,
            position = _node.GetPosition().position,
            endNodeType = _node.EndNodeType,
            payload = _node.Payload,

        };

        return nodeData;
    }
    #endregion
    #region Load stuff
    private void ClearGraph()
    {
        edges.ForEach(
            edge => graphView.RemoveElement(edge)
        );
        nodes.ForEach(
            node => graphView.RemoveElement(node)
        );
    }

    private void GenerateNodes(DialogContainerSO dialogContainer)
    {
        // Start
        foreach (StartNodeData node in dialogContainer.startNodeDatas)
        {
            StartNode tempNode = graphView.CreateStartNode(node.position);
            tempNode.NodeGuid = node.nodeGuid;

            graphView.AddElement(tempNode);
        }

        //End
        foreach (EndNodeData node in dialogContainer.endNodeDatas)
        {
            EndNode tempNode = graphView.CreateEndNode(node.position);
            tempNode.NodeGuid = node.nodeGuid;
            tempNode.EndNodeType = node.endNodeType;
            tempNode.Payload = node.payload;

            tempNode.LoadValueIntoField();

            graphView.AddElement(tempNode);
        }
        //Dialog
        foreach (DialogNodeData node in dialogContainer.dialogNodeDatas)
        {
            DialogNode tempNode = graphView.CreateDialogNode(node.position);
            tempNode.NodeGuid = node.nodeGuid;
            tempNode.SpeakerName = node.Name;
            tempNode.Text = node.Text;

            foreach (DialogNodePort nodePort in node.dialogNodePorts)
            {
                tempNode.AddChoicePort(tempNode, nodePort);
            }
            tempNode.LoadValueIntoField();
            graphView.AddElement(tempNode);
        }
    }

    private void ConnectNodes(DialogContainerSO dialogContainer)
    {
        for (int i = 0; i < nodes.Count(); i++)
        {
            List<NodeLinkData> connections = dialogContainer.nodeLinkDatas.Where(edge => edge.baseNodeGuid == nodes[i].NodeGuid).ToList();

            for (int j = 0; j < connections.Count(); j++)
            {
                string targetNodeGuid = connections[j].targetNodeGuid;
                BaseNode targetNode = nodes.First(node => node.NodeGuid == targetNodeGuid);

                if ((nodes[i] is DialogNode) == false)
                {
                    LinkNodesTogether(nodes[i].outputContainer[j].Q<Port>(), (Port)targetNode.inputContainer[0]);
                }
            }
        }

        List<DialogNode> dialogNodes = nodes.FindAll(node => node is DialogNode).Cast<DialogNode>().ToList();

        foreach (DialogNode dialogNode in dialogNodes)
        {
            foreach (DialogNodePort nodePort in dialogNode.DialogNodePorts)
            {
                if (nodePort.InputGuid != string.Empty)
                {
                    BaseNode targetNode = nodes.First(node => node.NodeGuid == nodePort.InputGuid);
                    LinkNodesTogether((Port) nodePort.MyPort, (Port)targetNode.inputContainer[0]);
                }
            }

        }

    }
    private void LinkNodesTogether(Port outputPort, Port inputPort)
    {
        Edge tempEdge = new Edge()
        {
            output = outputPort,
            input = inputPort
        };
        tempEdge.input.Connect(tempEdge);
        tempEdge.output.Connect(tempEdge);
        graphView.Add(tempEdge);
    }
    #endregion
}
