using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.UIElements;
using UnityEngine.UIElements;
using UnityEditor.Experimental.GraphView;

public class DialogEditorWindow : GraphViewEditorWindow
{
    private DialogContainerSO currentDialogContainer;
    private DialogGraphView graphView;
    private DialogSaveAndLoad saveAndLoad;
    private Label nameOfDialogContainer;
    [OnOpenAsset()]
    public static bool ShowWindow(int _instanceId, int line)
    {
        UnityEngine.Object item = EditorUtility.InstanceIDToObject(_instanceId);

        if (item is DialogContainerSO)
        {
            DialogEditorWindow window = (DialogEditorWindow)GetWindow(typeof(DialogEditorWindow));
            window.titleContent = new GUIContent("Dialog Editor");
            window.currentDialogContainer = item as DialogContainerSO;
            window.minSize = new Vector2(500, 250);
            window.Load();
        }

        return false;
    }

    private void OnEnable()
    {
        ConstructGraphView();
        GenerateToolbar();
        Load();
    }
    private void OnDisable()
    {
        rootVisualElement.Remove(graphView);
        Save();

    }
    private void ConstructGraphView()
    {
        graphView = new DialogGraphView(this);
        graphView.StretchToParentSize();
        MiniMap minimap = new MiniMap();
        minimap.BringToFront();
        minimap.visible = true;
        minimap.SetPosition(new Rect(new Vector2(10,50),new Vector2(250,150)));
        graphView.Add(minimap);
        rootVisualElement.Add(graphView);

        saveAndLoad = new DialogSaveAndLoad(graphView);
    }
    private void GenerateToolbar()
    {
        Toolbar toolbar = new Toolbar();
        StyleSheet styleSheet = Resources.Load<StyleSheet>("GraphViewStylesheet");
        rootVisualElement.styleSheets.Add(styleSheet);
        ToolbarButton saveBtn = new ToolbarButton
        {
            text = "Save"

        };
        saveBtn.clicked += () =>
        {
            //TODO: save dialog.
            Save();
        };
        toolbar.Add(saveBtn);

        ToolbarButton loadBtn = new ToolbarButton
        {
            text = "Load"

        };
        loadBtn.clicked += () =>
        {
            //TODO: save dialog.
            Load();
        };
        toolbar.Add(loadBtn);

        nameOfDialogContainer = new Label("");
        toolbar.Add(nameOfDialogContainer);
        nameOfDialogContainer.AddToClassList("nameOfDialogContainer");

        rootVisualElement.Add(toolbar);
    }
    private void Load()
    {
        if (currentDialogContainer != null)
        {
            nameOfDialogContainer.text = "Name:   " + currentDialogContainer.name;
            saveAndLoad.Load(currentDialogContainer);
        }

    }
    private void Save()
    {
        if (currentDialogContainer != null)
        {
            saveAndLoad.Save(currentDialogContainer);
        }
    }
}
