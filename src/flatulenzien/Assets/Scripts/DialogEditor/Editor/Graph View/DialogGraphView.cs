using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UIElements;

public class DialogGraphView : GraphView
{
    private string styleSheetName = "GraphViewStylesheet";
    private DialogEditorWindow editorWindow;
    private NodeSearchWindow searchWindow;
    public DialogGraphView(DialogEditorWindow _editorWindow)
    {
        editorWindow = _editorWindow;
        StyleSheet tmpStylesheet = Resources.Load<StyleSheet>(styleSheetName);
        styleSheets.Add(tmpStylesheet);

        SetupZoom(ContentZoomer.DefaultMinScale, ContentZoomer.DefaultMaxScale);
        this.AddManipulator(new ContentDragger());
        this.AddManipulator(new SelectionDragger());
        this.AddManipulator(new RectangleSelector());
        this.AddManipulator(new FreehandSelector());
    
        GridBackground grid = new GridBackground();
        Insert(0, grid);
        grid.StretchToParentSize();

        AddSearchWindow();
    }

    private void AddSearchWindow()
    {
        searchWindow = ScriptableObject.CreateInstance<NodeSearchWindow>();
        searchWindow.Configure(editorWindow, this);
        nodeCreationRequest = context => SearchWindow.Open(new SearchWindowContext(context.screenMousePosition), searchWindow);
    }

    public override List<Port> GetCompatiblePorts(Port startPort, NodeAdapter nodeAdapter)
    {
        List<Port> compatiblePorts = new List<Port>();
        Port startPortView = startPort;

        ports.ForEach((port) => 
        {
            Port portView = port;
            if (startPort != portView&& startPortView.node != portView.node && startPortView.direction != port.direction){
                compatiblePorts.Add(port);
            }

        });
        return compatiblePorts;
    }
    public StartNode CreateStartNode(Vector2 _pos)
    {
        StartNode tmp = new StartNode(_pos, editorWindow, this);
        return tmp;
    }
    public DialogNode CreateDialogNode(Vector2 _pos)
    {
        DialogNode tmp = new DialogNode(_pos, editorWindow, this);
        return tmp;
    }
    public EndNode CreateEndNode(Vector2 _pos)
    {
        EndNode tmp = new EndNode(_pos, editorWindow, this);
        return tmp;
    }
    
    }
