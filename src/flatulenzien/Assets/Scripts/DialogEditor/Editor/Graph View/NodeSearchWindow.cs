using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UIElements;

public class NodeSearchWindow : ScriptableObject, ISearchWindowProvider
{
    private DialogEditorWindow editorWindow;
    private DialogGraphView graphView;

    public void Configure(DialogEditorWindow _editorWindow, DialogGraphView _graphview)
    {
        editorWindow = _editorWindow;
        graphView = _graphview;
    }

    public List<SearchTreeEntry> CreateSearchTree(SearchWindowContext context)
    {
        List<SearchTreeEntry> tree = new List<SearchTreeEntry>
        {
            new SearchTreeGroupEntry(new GUIContent("Dialog Node"), 0),
            new SearchTreeGroupEntry(new GUIContent("Dialog"), 1),

            AddNodeSearch("Start Node", new StartNode()),
            AddNodeSearch("Dialog Node", new DialogNode()),
            AddNodeSearch("End Node", new EndNode()),
        };
        return tree;
    }
    private SearchTreeEntry AddNodeSearch( string _name, BaseNode _baseNode)
    {
        SearchTreeEntry tmp = new SearchTreeEntry(new GUIContent(_name))
        {
            level = 2,
            userData = _baseNode
        };

        return tmp;
    }

    public bool OnSelectEntry(SearchTreeEntry _SearchTreeEntry, SearchWindowContext _context)
    {
        Vector2 mousePosition = editorWindow.rootVisualElement.ChangeCoordinatesTo(
            editorWindow.rootVisualElement.parent, _context.screenMousePosition - editorWindow.position.position
        );

        Vector2 graphMousePosition = graphView.contentViewContainer.WorldToLocal(mousePosition);
        return CheckForNodeType(_SearchTreeEntry, graphMousePosition);
    }

    private bool CheckForNodeType(SearchTreeEntry _searchTreeEntry, Vector2 _pos)
    {
        switch (_searchTreeEntry.userData)
        {
            case StartNode node:
                graphView.AddElement(graphView.CreateStartNode(_pos));
                //Make Start Node
                return true;
            case DialogNode node:
                graphView.AddElement(graphView.CreateDialogNode(_pos));

                //Make Start Node
                return true;
            case EndNode node:
                graphView.AddElement(graphView.CreateEndNode(_pos));

                //Make Start Node
                return true;

            default:
                break;
        }
        return false;
    }
}
