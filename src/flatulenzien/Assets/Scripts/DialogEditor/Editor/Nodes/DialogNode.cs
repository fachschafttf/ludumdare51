using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor.Experimental.GraphView;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

public class DialogNode : BaseNode
{
    private string text;
    private string speakerName = "";

    private List<DialogNodePort> dialogNodePorts = new List<DialogNodePort>();


    public string Text { get => text; set => text = value; }
    public string SpeakerName { get => speakerName; set => speakerName = value; }
    public List<DialogNodePort> DialogNodePorts { get => dialogNodePorts; set => dialogNodePorts = value; }

    private TextField speakerName_Field;
    private TextField  texts_Field;

    public DialogNode()
    {

    }

    public DialogNode(Vector2 _position, DialogEditorWindow _editorWindow, DialogGraphView _graphView)
    {
        editorWindow = _editorWindow;
        graphView = _graphView;
        outputContainer.AddToClassList("DialogOutput");
        inputContainer.AddToClassList("DialogInput");

        title = "Dialog";
        text = "";
        SetPosition(new Rect(_position, defaultNodeSize));
        nodeGuid = Guid.NewGuid().ToString();

        AddInputPort("Input", Port.Capacity.Multi);

        //Name Text
        Label label_name = new Label("Name");
        label_name.AddToClassList("label_name");
        label_name.AddToClassList("Label");
        mainContainer.Add(label_name);

        speakerName_Field = new TextField("");
        speakerName_Field.RegisterValueChangedCallback(value => 
        {
            speakerName = value.newValue;
        });
        speakerName_Field.SetValueWithoutNotify(speakerName);
        speakerName_Field.AddToClassList("TextName");
        mainContainer.Add(speakerName_Field);
        
        //TextBox
        Label label_texts = new Label("Text Box");
        label_texts.AddToClassList("label_text");
        label_texts.AddToClassList("Label");
        mainContainer.Add(label_texts);

        texts_Field = new TextField("");
        texts_Field.RegisterValueChangedCallback(value => 
        {
            text = value.newValue;
        });
        texts_Field.SetValueWithoutNotify(text);
        texts_Field.multiline = true;
        texts_Field.AddToClassList("TextBox");
        mainContainer.Add(texts_Field);

        Button button = new Button(() => AddChoicePort(this))
        {
            text = "Add Choice",
        };


        titleButtonContainer.Add(button);

    }
    
    public override void LoadValueIntoField()
    {
        texts_Field.SetValueWithoutNotify(text);
        speakerName_Field.SetValueWithoutNotify(speakerName);

    }
    
    public Port AddChoicePort(BaseNode _baseNode, DialogNodePort _dialogNodePoint = null)
    {
        Port port = GetPortInstance(Direction.Output, Port.Capacity.Single);
        int outputPortCount = _baseNode.outputContainer.Query("connector").ToList().Count();
        string outputPortName = $"Choice {outputPortCount + 1 }";

        DialogNodePort dialogNodePort = new DialogNodePort();
        dialogNodePort.nodePortText = outputPortName;

        if(_dialogNodePoint != null)
        {
            dialogNodePort.InputGuid = _dialogNodePoint.InputGuid;
            dialogNodePort.OutputGuid = _dialogNodePoint.OutputGuid;
            
            dialogNodePort.nodePortText = _dialogNodePoint.nodePortText;
        }

        //Text for the port description

        dialogNodePort.TextField = new TextField();
        dialogNodePort.TextField.RegisterValueChangedCallback(value => 
        {
            dialogNodePort.nodePortText = value.newValue;
        });
        dialogNodePort.TextField.SetValueWithoutNotify( dialogNodePort.nodePortText);
        port.contentContainer.Add(dialogNodePort.TextField);
        //Delete button
        Button deleteButton = new Button(() => DeletePort(_baseNode, port))
        {
            text = "X",
        };
        port.contentContainer.Add(deleteButton);
        dialogNodePort.MyPort = port;
        port.portName = "";

        dialogNodePorts.Add(dialogNodePort);
        _baseNode.outputContainer.Add(port);
        //Refresh
        _baseNode.RefreshPorts();
        _baseNode.RefreshExpandedState();

        return port;
    } 

    private void DeletePort(BaseNode _node, Port _port)
    {
        DialogNodePort tmp = dialogNodePorts.Find(port => port.MyPort == _port);
        dialogNodePorts.Remove(tmp);

        IEnumerable<Edge> portEdge = graphView.edges.ToList().Where(edge => edge.output == _port);
        if (portEdge.Any())
        {
            Edge edge = portEdge.First();
            edge.input.Disconnect(edge);
            edge.output.Disconnect(edge);
            graphView.RemoveElement(edge);
        }
        _node.outputContainer.Remove(_port);
        
        _node.RefreshPorts();
        _node.RefreshExpandedState();

    }
}
