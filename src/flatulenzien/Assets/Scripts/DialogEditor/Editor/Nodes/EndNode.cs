using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UIElements;

public class EndNode : BaseNode
{
    private EndNodeType endNodeType = EndNodeType.End;
    private EnumField enumField;

    public EndNodeType EndNodeType { get => endNodeType; set => endNodeType = value; }
    private string payload;
    public string Payload { get => payload; set => payload = value; }
    private TextField payloadField;

    
    public EndNode()
    {

    }

    public EndNode(Vector2 _position, DialogEditorWindow _editorWindow, DialogGraphView _graphView)
    {
        editorWindow = _editorWindow;
        graphView = _graphView;

        title = "End";
        SetPosition(new Rect(_position, defaultNodeSize));
        nodeGuid = Guid.NewGuid().ToString();

        AddInputPort("Input", Port.Capacity.Multi);

        enumField = new EnumField()
        {
            value = endNodeType,
        };
        enumField.Init(endNodeType);
        enumField.RegisterValueChangedCallback((value) =>
        {
            endNodeType = (EndNodeType)value.newValue;
        });
        enumField.SetValueWithoutNotify(endNodeType);
        mainContainer.Add(enumField);

        payloadField = new TextField("Endnode Payload (string)");
        payloadField.RegisterValueChangedCallback(value => 
        {
            payload = value.newValue;
        });
        payloadField.SetValueWithoutNotify(payload);
        mainContainer.Add(payloadField);

        
    }
    public override void LoadValueIntoField()
    {
        enumField.SetValueWithoutNotify(endNodeType);
        payloadField.SetValueWithoutNotify(payload);
    }
}
