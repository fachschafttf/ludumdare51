using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class StartNode : BaseNode
{
    public StartNode()
    {

    }

    public StartNode(Vector2 _position, DialogEditorWindow _editorWindow, DialogGraphView _graphView)
    {
        editorWindow = _editorWindow;
        graphView = _graphView;
        
        title = "Start";
        SetPosition(new Rect(_position, defaultNodeSize));
        nodeGuid = Guid.NewGuid().ToString();

        AddOutputPort("Output");

        RefreshExpandedState();
        RefreshPorts();
    }
}
