using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

[CreateAssetMenu(menuName ="Dialog/New Dialog")]
[System.Serializable]
public class DialogContainerSO : ScriptableObject
{
    public List<NodeLinkData> nodeLinkDatas = new List<NodeLinkData>();
    public List<DialogNodeData> dialogNodeDatas = new List<DialogNodeData>();
    public List<EndNodeData> endNodeDatas = new List<EndNodeData>();
    public List<StartNodeData> startNodeDatas = new List<StartNodeData>();
    
    public List<BaseNodeData> allNodes
    {
        get
        {
            List<BaseNodeData> tmp = new List<BaseNodeData>();
            tmp.AddRange(dialogNodeDatas);
            tmp.AddRange(endNodeDatas);
            tmp.AddRange(startNodeDatas);

            return tmp;
        }
    }
}
[System.Serializable]
public class NodeLinkData
{
    public string baseNodeGuid;
    public string targetNodeGuid;
}
[System.Serializable]
public class BaseNodeData
{
    public string nodeGuid;
    public Vector2 position;
}
[System.Serializable]
public class DialogNodeData : BaseNodeData
{
    public List<DialogNodePort> dialogNodePorts;
    public string Name;
    public string Text;
}
[System.Serializable]
public class EndNodeData: BaseNodeData
{
    public EndNodeType endNodeType;
    //public TextField PayloadField;
    public string payload;
}
[System.Serializable]
public class StartNodeData: BaseNodeData
{
}

[System.Serializable]
public class DialogNodePort
{
    public string InputGuid;
    public string OutputGuid;
    public System.Object MyPort;
    public TextField TextField;
    public string nodePortText;
}