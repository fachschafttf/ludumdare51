using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EndNodeType 
{
    End,
    Repeat,
    GoBack,
    ReturnToStart,
    StartFight,
    OpenLoadout,
    ChooseStarter,
    GetWildMon,
    HealPlayer,
    Respawn,
}
