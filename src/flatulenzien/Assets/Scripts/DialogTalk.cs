using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DialogTalk : DialogDataReciever
{
    [SerializeField] private DialogController dialogController;

    private DialogNodeData currentDialogNodeData;
    private DialogNodeData lastDialogNodeData;

    public void Awake()
    {
        dialogController = FindObjectOfType<DialogController>();
    }

    public void StartDialog()
    {
        CheckNodeType(GetNextNode(dialogContainer.startNodeDatas[0]));
        dialogController.ShowDialog(true);
    }

    private void CheckNodeType(BaseNodeData _baseNodeData)
    {
        switch (_baseNodeData)
        {
            case StartNodeData nodeData:
                RunNode(nodeData);
                break;
            case EndNodeData nodeData:
                RunNode(nodeData);
                break;
            case DialogNodeData nodeData:
                RunNode(nodeData);
                break;
            default:
                break;
        }
    }
    private void RunNode(StartNodeData _nodeData)
    {
        CheckNodeType(GetNextNode(dialogContainer.startNodeDatas[0]));
    }
    private void RunNode(EndNodeData _nodeData)
    {
        var gameManager = dialogController.gameManager.GetComponent<GameManager>();
        switch (_nodeData.endNodeType)
        {
            case EndNodeType.End:
                dialogController.ShowDialog(false);
                gameManager.CloseDialog();
                break;
            case EndNodeType.Repeat:
                CheckNodeType(GetNodeByGuid(currentDialogNodeData.nodeGuid));
                break;
            case EndNodeType.GoBack:
                CheckNodeType(GetNodeByGuid(lastDialogNodeData.nodeGuid));
                break;
            case EndNodeType.ReturnToStart:
                CheckNodeType(GetNextNode(dialogContainer.startNodeDatas[0]));
                break;
            case EndNodeType.StartFight:
                dialogController.ShowDialog(false);
                gameManager.TriggerFight();
                break;
            case EndNodeType.OpenLoadout:
                dialogController.ShowDialog(false);
                gameManager.OpenCenter();
                break;
            case EndNodeType.ChooseStarter:
                dialogController.ShowDialog(false);
                gameManager.TriggerSelectStarter(_nodeData.payload);
                break;
            case EndNodeType.GetWildMon:
                dialogController.ShowDialog(false);
                gameManager.GetWildMon(_nodeData.payload);
                break;
            case EndNodeType.HealPlayer:
                dialogController.ShowDialog(false);
                gameManager.TriggerHealTeam();
                break;
            case EndNodeType.Respawn:
                dialogController.ShowDialog(false);
                gameManager.TriggerRespawn();
                break;
            default:
                break;
        }

    }
    private void RunNode(DialogNodeData _nodeData)
    {
        lastDialogNodeData = currentDialogNodeData;
        currentDialogNodeData = _nodeData;

        dialogController.SetText(_nodeData.Name, _nodeData.Text);
        MakeButtons(_nodeData.dialogNodePorts);
    }

    private void MakeButtons(List<DialogNodePort> _nodePorts)
    {
        List<string> texts = new List<string>();
        List<UnityAction> unityActions = new List<UnityAction>();

        foreach (DialogNodePort nodePort in _nodePorts)
        {
            texts.Add(nodePort.nodePortText);
            UnityAction tempAction = null;
            tempAction += () =>
            {
                CheckNodeType(GetNodeByGuid(nodePort.InputGuid));
            };
            unityActions.Add(tempAction);
        }

        dialogController.SetButtons(texts, unityActions);
    }
}
