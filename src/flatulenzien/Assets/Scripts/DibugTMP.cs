using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DibugTMP : MonoBehaviour
{
    public CombatSystem combatSystem;
    public EnemyTeam enemyTeam;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetupBattle()
    {
        combatSystem.SetupBattle(enemyTeam);
    }
}
