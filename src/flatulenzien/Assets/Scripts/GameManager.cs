using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject player;
    public MobileController mobileController;
    public static GameManager Instance;
    public GameObject dialogController;
    public GameObject CenterUI;
    public CombatSystem combatSystem;
    public DialogContainerSO initialDialog;
    public WorldManager worldManager;

    private PlayerController playerController;
    private Interaction currentInteraction = null;

    [SerializeField]
    private static string PathToMonster = "";

    public static class MonsterLoader
    {
        public static Monster LoadMonsterByName(string correctMonsterNameFromRessources)
        {
            string PATH = GameManager.PathToMonster;
            if (PATH == "")
            {
                PATH = "Monster/";
            }
            var mon = Resources.Load<Monster>(PATH + correctMonsterNameFromRessources);
            return mon;
        }
    }

    public bool Paused;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }
    }


    // Start is called before the first frame update
    void Start()
    {
        OpenDialog(initialDialog);
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void SubscribePlayer(PlayerController playerController)
    {
        this.playerController = playerController;
    }

    public void OpenDialog(DialogContainerSO dialog)
    {
        playerController.StartInputBlock();
        Debug.Log("open dialog");
        dialogController.GetComponent<DialogTalk>().SetDialog(dialog);
        dialogController.GetComponent<DialogTalk>().StartDialog();

    }


    public void CloseDialog()
    {
        playerController.StopInputBlock();
    }

    public void OpenFight(EnemyTeam enemyTeam)
    {
        playerController.StartInputBlock();
        Debug.Log("open fight");
        combatSystem.SetupBattle(enemyTeam);
    }

    public void TriggerFight()
    {
        if(currentInteraction != null && currentInteraction is OpenFight)
        {
            OpenFight(((OpenFight)currentInteraction).enemyTeam);
        }
    }

    public void TriggerSelectStarter(string starterName)
    {
        player.GetComponent<PlayerTeam>().ChangeTeam(new List<Monster>() { MonsterLoader.LoadMonsterByName(starterName) });
        ((OpenStaterDialog)currentInteraction).OpenStaterResponse(starterName);
        worldManager.UpdateWorld();
    }

    public void TriggerHealTeam()
    {
        player.GetComponent<PlayerTeam>().HealTeam();
        playerController.StopInputBlock();
    }


    public void FinishedFight(BattleState battlestate)
    {
        CloseFight();
        if (battlestate == BattleState.WON)
        {
            // Destroy(currentInteraction.gameObject);
            ((OpenFight)currentInteraction).OutcomeDialog(battlestate);
        }
        if (battlestate == BattleState.LOST)
        {
            ((OpenFight)currentInteraction).OutcomeDialog(battlestate);
        }
    }

    public void TriggerRespawn()
    {
        player.transform.position = new Vector3(0.5f, -1.5f, 0);
        player.GetComponent<PlayerTeam>().HealTeam();
    }

    public void CloseFight()
    {
        playerController.StopInputBlock();
    }

    public void OpenCenter()
    {
        playerController.StartInputBlock();
        Debug.Log("open Center");
        CenterUI.GetComponent<CenterController>().OpenCenter();
    }

    public void CloseCenter()
    {
        playerController.StopInputBlock();
        CenterUI.GetComponent<CenterController>().CloseCenter();
    }

    public void AssignInteraction(Interaction interaction)
    {
        currentInteraction = interaction;
    }

    internal void GetWildMon(string payload)
    {
        player.GetComponent<PlayerTeam>().AddNewMonsterToAvailableMonster(MonsterLoader.LoadMonsterByName(payload));
        playerController.StopInputBlock();
        currentInteraction.gameObject.SetActive(false);
        worldManager.UpdateWorld();
    }
}
