using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Interaction : MonoBehaviour
{
    public GameObject owner;
    protected GameManager gameManager;
    // Start is called before the first frame update
    void Start()
    {
        owner = gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Execute()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        gameManager.AssignInteraction(this);
        PerformInteraction();
    }


    protected abstract void PerformInteraction();

    public virtual void Callback() { }

}
