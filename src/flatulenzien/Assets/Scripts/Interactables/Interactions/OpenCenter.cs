using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenCenter : Interaction
{
    public DialogContainerSO dialog;
    protected override void PerformInteraction()
    {
        gameManager.OpenDialog(dialog);
    }
}
