using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDialogAndFight : OpenFight
{
    public DialogContainerSO dialog;

    protected override void PerformInteraction()
    {
        gameManager.OpenDialog(dialog);
    }
}
