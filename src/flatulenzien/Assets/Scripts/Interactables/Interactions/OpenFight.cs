using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenFight : Interaction
{
    public EnemyTeam enemyTeam;
    public DialogContainerSO wonDialog;
    public DialogContainerSO lostDialog;

    protected override void PerformInteraction()
    {
        gameManager.OpenFight(enemyTeam);
    }

    public virtual void OutcomeDialog(BattleState state)
    {
        if (state == BattleState.WON)
        {
            gameManager.OpenDialog(wonDialog);
        }
        if (state == BattleState.LOST)
        {
            gameManager.OpenDialog(lostDialog);
        }
    }
}
