using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenStaterDialog : Interaction
{
    [Serializable]
    public struct StaterResponse
    {
        public string staterName;
        public DialogContainerSO dialog;
    }

    public DialogContainerSO dialog;
    public StaterResponse[] staterResponses;

    protected override void PerformInteraction()
    {
        gameManager.OpenDialog(dialog);
    }

    public void OpenStaterResponse(string staterName)
    {
        foreach (StaterResponse staterResponse in staterResponses)
        {
            if (staterResponse.staterName == staterName)
            {
                gameManager.OpenDialog(staterResponse.dialog);
            }
        }
        gameObject.SetActive(false);
    }
}
