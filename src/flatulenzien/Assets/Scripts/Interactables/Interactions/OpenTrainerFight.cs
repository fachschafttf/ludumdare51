using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenTrainerFight : OpenFight
{
    public DialogContainerSO dialog;
    public GameObject toRemove;

    protected override void PerformInteraction()
    {
        gameManager.OpenDialog(dialog);
    }

    public override void OutcomeDialog(BattleState state)
    {
        if (state == BattleState.WON)
        {
            gameManager.OpenDialog(wonDialog);
            toRemove.SetActive(false);
        }
        if (state == BattleState.LOST)
        {
            gameManager.OpenDialog(lostDialog);
        }
    }
}
