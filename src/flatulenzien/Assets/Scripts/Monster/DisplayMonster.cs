using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DisplayMonster : MonoBehaviour
{
    // scriptable object
    public Monster Monster;

    // sprite of the gameobject
    public Image image;

    public TextMeshProUGUI text;    

    private bool showDetails;

    void Start()
    {
        InitImage();
        InitDetails();
    }

    /// <summary>
    /// Artwork
    /// </summary>
    private void InitImage()
    {
        if (image == null)
        {
            image = GetComponentInChildren<Image>();
            if (image == null)
            {
                return;
            }
        }
        {
            image.sprite = Monster.Artwork;
        }
    }

    /// <summary>
    /// Description
    /// </summary>
    private void InitDetails()
    {
        if (text == null)
        {
            text = GetComponentInChildren<TextMeshProUGUI>();
            if (text == null)
            {
                return;
            }
        }
        {
            text.text = Monster.Description;
        }
    }

    private void ShowDetails()
    {
        showDetails = true;
    }

    private void HideDetails()
    {
        showDetails = false;
    }
}
