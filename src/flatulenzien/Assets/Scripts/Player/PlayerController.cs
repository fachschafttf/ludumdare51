using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour, MainInput.IWorldPlayerActions
{
    public WorldManager worldManager;

    private MainInput controls;
    private PlayerMovement playerMovement;

    private bool blockInput = false;

	private void Awake()
	{
        if (controls == null)
        {
            controls = new MainInput();
            controls.WorldPlayer.SetCallbacks(this);
        }
        if (worldManager == null)
            worldManager = GameObject.Find("Grid").GetComponent<WorldManager>();
        playerMovement = gameObject.GetComponent<PlayerMovement>();
        playerMovement.Initialize(worldManager);

        GameObject.Find("GameManager").GetComponent<GameManager>().SubscribePlayer(this);
	}

	private void OnEnable()
	{
        controls.Enable();
	}

	private void OnDisable()
	{
        controls.Disable();
	}

	// Start is called before the first frame update
	void Start()
    {
        // controls.Main.Movement.performed += ctx => Move(ctx.ReadValue<Vector2>());
        MobileController mobileController = GetController();
        if (mobileController != null)
        {
            mobileController.enableControls();
        }
    }

    void FixedUpdate()
    {
        playerMovement.Move();
    }


    // Grab Movement input from input system.
    public void OnPlayerMovement(InputAction.CallbackContext context)
    {
        if (blockInput)
            return;
        // Possible with Vector2
        Vector2 moveInput = context.ReadValue<Vector2>();
        playerMovement.MoveInputUpdate(moveInput);
    }

    public void OnInteract(InputAction.CallbackContext context)
    {
        if (blockInput)
            return;
        GameObject interactable = worldManager.GetInteractableObjectAt(playerMovement.GetPosInFront());
        if (interactable != null)
        {
            Debug.Log("Interact with " + interactable.transform.name);
            Interaction interaction = interactable.GetComponent<Interaction>();
            if (interaction != null)
                interaction.Execute();
        }
    }


    private MobileController GetController()
    {
        MobileController ret = GameObject.Find("MobileController")?.GetComponent<MobileController>();
        return ret;
    }

    public void StartInputBlock()
    {
        playerMovement.MoveInputUpdate(Vector2.zero);
        MobileController mobileController = GetController();
        if (mobileController != null) {
            mobileController.disableControls();
        }
        blockInput = true;
    }

    public void StopInputBlock()
    {
        MobileController mobileController = GetController();
        if (mobileController != null)
        {
            mobileController.enableControls();
        }
        blockInput = false;
    }
}
