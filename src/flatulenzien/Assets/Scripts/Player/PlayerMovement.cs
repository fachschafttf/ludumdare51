using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class PlayerMovement : MonoBehaviour
{

    public float movementSpeed = 3f;
    public Transform movePoint;
    private Vector3 movement;
    private Vector2 moveInput = Vector2.zero;
    private Vector2 previousInput = Vector2.zero;

    private Tilemap groundTilemap;
    private Tilemap collisionTilemap;
    private Vector2 lookDirection = Vector2.down;

    private Animator animator;
    private WorldManager worldManager;

    // Start is called before the first frame update
    void Start()
    {
        movePoint.parent = null;
    }

    private void Awake()
    {
        animator = GetComponentInChildren<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public Vector3 GetPosInFront()
    {
        return transform.position + (Vector3)lookDirection;
    }


    public void Initialize(WorldManager worldManager)
    {
        this.worldManager = worldManager; 
    }

    public void Move()
    {
        
        if (Vector3.Distance(transform.position, movePoint.position) < .01f)
        {
            UpdateMovePoint();
        }

        
        transform.position = Vector3.MoveTowards(transform.position, movePoint.position, Time.deltaTime * movementSpeed);
    }

    private void UpdateMovePoint()
    {
        if (moveInput.sqrMagnitude > 0.1f && worldManager.CanMoveTo(transform.position + (Vector3)moveInput))
        {
            movePoint.position = worldManager.GetTileMiddle(transform.position + (Vector3)moveInput);
            animator.SetBool("IsWalking", true);

            lookDirection = Vector3.Normalize(movePoint.position - transform.position);
            // lookDirection = moveInput;
            animator.SetFloat("X", lookDirection.x);
            animator.SetFloat("Y", lookDirection.y);
            
        } else
        {
            animator.SetBool("IsWalking", false);
            if (moveInput.sqrMagnitude > 0.1f)
            {
                lookDirection = Vector3.Normalize(moveInput);
                //lookDirection = moveInput;
                animator.SetFloat("X", lookDirection.x);
                animator.SetFloat("Y", lookDirection.y);
            }
        }
    }

    // Grab Movement input from input system.
    public void MoveInputUpdate(Vector2 moveInput)
    {
        moveInput = moveInput - previousInput * 0.01f;
        if (moveInput.sqrMagnitude < 0.1f)
        {
            moveInput = Vector3.zero;
        }
        else
        {
            if (Mathf.Abs(moveInput.x) > Mathf.Abs(moveInput.y))
            {
                moveInput.y = 0;
            }
            else
            {
                moveInput.x = 0;
            }
            moveInput = moveInput.normalized;
            // UpdateMovePoint();
           
        }
        this.moveInput = moveInput;
        previousInput = moveInput;
    }
}
