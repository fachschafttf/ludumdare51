using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new Abilitie", menuName = "Monster and Abilitie/Abilitie")]
public class Abilitie : ScriptableObject
{
    public string AbilitieName;
    public string Description;
    public Sprite Artwork;

    public int CastTime;
    public List<CardEffect> Effects;

    // possible target types:
    // self, all friends, all enemies, first enemy, last enemy, ALL
    // => list of targets with those keywords?
    // the individual targets are of shape 2^0 2^1 2^2 .... 2^5
    // addressing multiple enemies is the sum of the indiviudal ones
    // kind of unneccessary detail probably, but it is beautiful math
    public enum Target
    {
        // with [Display("using System.ComponentModel.DataAnnotations;")] we could get rid of those ugly things
        // which also pop up in switch cases
        // but i need to install that packet first, and i dont know if thats a problem for other people
        // that also dont have it installed....
        // .... lets keep it ugly instead of screwing the jam team

        XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX = -1,
        
        EXPLICIT_TARGETING_MISSES_WHEN_SLOTS_ARE_EMPTY = -2,
        EnemyOnSlot1 = 1,
        EnemyOnSlot2 = 2,
        EnemyOnSlot3 = 4,
        AllyOnSlot1 = 8,
        AllyOnSlot2 = 16,
        AllyOnSlot3 = 32,
        AllEnemies = 7,
        AllAllies = 56,
        AllEverything = 63,
        XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXO = -3,

        SMART_TARGETING = -4,
        Self = 0,
        LastEnemy = 64 + 4,
        LastAlly = 64 + 32,
        FirstAndLastEnemy = 64 + 1 + 4,
        FirstAndLastAlly = 64 + 8 + 32,

        XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXOX = -5,
    }

    public enum InteractionType
    {
        Damage = 0,
        Heal = 1,
    }

    /// <summary>
    /// apply all the effects, moved this function here so we dont accidentaly deal the only enemy damage twice
    /// (e.g. for effects that hit first and lanst enemy)
    /// </summary>
    /// <param name="allyList"></param>
    /// <param name="enemyList"></param>
    public void ApplyEffects(CombatMonster self, List<CombatMonster> allyList, List<CombatMonster> enemyList)
    {
        // no jesus stuff atm
        if (self.hasDied)
        {
            return;
        }

        foreach (var effect in Effects)
        {
            List<CombatMonster> realTargets = CalculateTargets(self, allyList, enemyList, effect.Target);
            foreach (var target in realTargets)
            {
                if (target.InteractHP(effect.InteractionType, effect.Value))
                {
                    target.ShouldDie();
                }

                //// switching to make adding buffs easier
                //switch (effect.InteractionType)
                //{
                //    case InteractionType.Damage:
                //        target.InteractHP(effect.InteractionType, effect.Value);
                //        break;
                //    case InteractionType.Heal:
                //        target.InteractHP(effect.InteractionType, effect.Value);
                //        break;
                //    default:
                //        break;
                //}
            }
        }
    }

    private static List<CombatMonster> CalculateTargets(CombatMonster self, List<CombatMonster> allyList, List<CombatMonster> enemyList, Target target)
    {
        var enemyCount = enemyList.Count;
        var allyCount = allyList.Count;
        var targets = new List<CombatMonster>();

        // explicit targeting
        if ((int)target > 0 && (int)target < 64)
        {
            switch (target)
            {
                case Target.EnemyOnSlot1:
                    targets.Add(enemyList[0]);
                    break;
                case Target.EnemyOnSlot2:
                    if (enemyCount > 1)
                    {
                        targets.Add(enemyList[1]);
                    }
                    break;
                case Target.EnemyOnSlot3:
                    if (enemyCount > 2)
                    {
                        targets.Add(enemyList[2]);
                    }
                    break;
                case Target.AllyOnSlot1:
                    targets.Add(allyList[0]);
                    break;
                case Target.AllyOnSlot2:
                    if (allyCount > 1)
                    {
                        targets.Add(allyList[1]);
                    }
                    break;
                case Target.AllyOnSlot3:
                    if (allyCount > 2)
                    {
                        targets.Add(allyList[2]);
                    }
                    break;
                case Target.AllEnemies:
                    targets.AddRange(enemyList);
                    break;
                case Target.AllAllies:
                    targets.AddRange(allyList);
                    break;
                case Target.AllEverything:
                    targets.AddRange(enemyList);
                    targets.AddRange(allyList);
                    break;
                default:
                    Debug.LogError($"bad targeting of explicit effect, this should not happen -> bug? or maybe someone clicked on wrong enum in unity");
                    break;
            }
        }

        // smart targeting
        else if ((int)target >= 64)
        {
            switch (target)
            {
                case Target.Self:
                    targets.Add(self);
                    break;
                case Target.LastEnemy:
                    targets.Add(LastMonster(enemyList));
                    break;
                case Target.LastAlly:
                    targets.Add(LastMonster(allyList));
                    break;
                case Target.FirstAndLastEnemy:
                    targets.AddRange(FirstAndLastMonster(enemyList));

                    break;
                case Target.FirstAndLastAlly:
                    targets.AddRange(FirstAndLastMonster(allyList));
                    break;
                default:
                    Debug.LogError($"bad targeting of smart effect, this should not happen -> bug? or maybe someone clicked on wrong enum in unity");
                    break;
            }
        }

        return targets;
    }

    internal void Starting()
    {
        // Literally do nothing for now!
        // Here we can make a sound
        // or play an animation etc.
    }

    private static CombatMonster LastMonster(List<CombatMonster> monsterList)
    {
        int size = monsterList.Count;
        return monsterList[size - 1];
    }

    /// <summary>
    /// does not target the same monster twice if its alone
    /// </summary>
    /// <param name="monsterList"></param>
    /// <returns></returns>
    private static List<CombatMonster> FirstAndLastMonster(List<CombatMonster> monsterList)
    {
        // add the first monster
        var targets = new List<CombatMonster>
        {
            monsterList[0]
        };

        // only add the last monster if its different
        if (monsterList.Count > 1)
        {
            targets.Add(LastMonster(monsterList));
        }
        return targets;
    }
}
