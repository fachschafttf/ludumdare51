using UnityEngine;

[CreateAssetMenu(fileName = "new EnemyTeam", menuName = "Monster and Abilitie/EnemyTeam")]
public class EnemyTeam : ScriptableObject
{
    public string TeamName;
    public string Description;
    public MonsterTeam Team;
}
