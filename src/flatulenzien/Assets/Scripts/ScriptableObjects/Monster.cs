using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new Monster", menuName = "Monster and Abilitie/Monster")]
public class Monster : ScriptableObject
{
    public string MonsterName;
    public string Description;
    public Sprite Artwork;

    public int ElementType; // todo: type of this
    public int Health;

    public List<Abilitie> Abilities;
}
