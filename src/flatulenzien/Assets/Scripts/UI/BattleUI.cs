using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BattleUI : MonoBehaviour, IPointerClickHandler
{
    public GameObject enemyMarkerPrefab;
    public GameObject playerMarkerPrefab;
    public GameObject timeSlotPrefab;
    public GameObject cardPrefab;

    public CombatSystem combatSystem;

    private GameObject cardInfo;
    private GameObject slotHighlight;
    private Transform payerMarkerSpace;
    public Transform enemyMarkerSpace;
    public Image Progressbar;


    private List<GameObject> playerMarkers = new List<GameObject>();
    private List<GameObject> enemyMarkers = new List<GameObject>();
    private List<GameObject> handCards = new List<GameObject>();

    private bool placing;
    private int toPlaceSize = 1;
    private int targetTimeSlot = 0;
    private int placingCardIndex = 0;

    // Start is called before the first frame update
    void Start()
    {
        cardInfo = transform.Find("CardInfo").gameObject;
        Transform timeLine = transform.Find("Main/TimeLine");
        slotHighlight = transform.Find("Main/TimeLine/SlotHighlight").gameObject;
        payerMarkerSpace = transform.Find("Main/TimeLine/PlayerMarkers");
        if (enemyMarkerSpace == null)
        {
            enemyMarkerSpace = transform.Find("Main/TimeLine/EnemyMarkers");
        }
        if (Progressbar == null)
        {
            Progressbar = transform.Find("Main/TimeLine/Progressbar").GetComponent<Image>();
        }



        cardInfo.SetActive(false);
        slotHighlight.SetActive(false);
        for (int i = 0; i < 10; i++)
        {
            GameObject timeSlot = Instantiate(timeSlotPrefab, timeLine);
            timeSlot.GetComponent<RectTransform>().anchoredPosition = new Vector2(50 + 150 * i, 0);
            timeSlot.GetComponent<TimeSlot>().mySlotNr = i;
        }
    }

    //TODO Zachis Timeline funktion verwenden
    private bool CanPlaceCard()
    {
        return combatSystem.CanPLaceCard(targetTimeSlot, placingCardIndex);
    }

    // Update is called once per frame
    void Update()
    {
        if (placing)
        {
            slotHighlight.GetComponent<RectTransform>().anchoredPosition = new Vector2(50 + 150 * targetTimeSlot, 20);
            if (CanPlaceCard())
            {
                slotHighlight.GetComponent<Image>().color = UnityEngine.Color.green;
            } else
            {
                slotHighlight.GetComponent<Image>().color = UnityEngine.Color.red;
            }
        }
    }

    public IEnumerator Progress()
    {
        int totalDistance = 1500;
        float unitsToMovePerSecond = totalDistance / 10f;

        while (true)
        {
            float timePassed = Time.deltaTime;
            var distToMoveNow = timePassed * unitsToMovePerSecond;
            var oldValue = Progressbar.rectTransform.sizeDelta;
            var newX = oldValue.x + distToMoveNow;
            Progressbar.rectTransform.sizeDelta = new Vector2(newX, oldValue.y);
            if (newX > totalDistance)
            {
                break;
            }
            yield return null;
        }
        yield return null;
    }

    public void StartProgressbar()
    {
        IEnumerator progressbar = Progress();
        StartCoroutine(progressbar);
    }

    private void ResetProgressbar()
    {
        StopAllCoroutines();
        var oldValue = Progressbar.rectTransform.sizeDelta;
        Progressbar.rectTransform.sizeDelta = new Vector2(0, oldValue.y);
    }

    public void StartRound()
    {
        /*TODO
         * Karten laden
         * karten spawnen (child von Cards, Prefab)
         */

        ResetProgressbar();

        foreach(GameObject card in handCards)
        {
            Destroy(card);
        }
        foreach (GameObject marker in playerMarkers)
        {
            Destroy(marker);
        }
        foreach (GameObject marker in enemyMarkers)
        {
            Destroy(marker);
        }

        enemyMarkers = new List<GameObject>();
        enemyMarkers = InstantiateEnemyMarkers();

        playerMarkers = new List<GameObject>();
        handCards = new List<GameObject>();

        Transform cardHand = transform.Find("CardHolder/Cards");
        for (int i = 0; i < combatSystem.playerHand.Count; i++)
        {
            GameObject card = Instantiate(cardPrefab, cardHand);
            card.GetComponent<CardUi>().Initialize(this, combatSystem.playerHand[i], i);
            handCards.Add(card);
        }
        if(cardInfo)
            cardInfo.SetActive(false);
        if(slotHighlight)
            slotHighlight.SetActive(false);
        placing = false;
    }

    private List<GameObject> InstantiateEnemyMarkers()
    {
        List<GameObject> marker = new List<GameObject>();
        int slotNr = 0;

        foreach (var abi in combatSystem.timeline.EnemyElements)
        {
            /*
            GameObject playerMarker = Instantiate(playerMarkerPrefab, payerMarkerSpace);
            playerMarker.GetComponent<RectTransform>().anchoredPosition = new Vector2(50 + 150 * slotNr, 30);
            playerMarker.GetComponent<RectTransform>().sizeDelta = new Vector2(150 * toPlaceSize, 90);
            playerMarker.GetComponent<PlayerMarker>().Initialize(targetTimeSlot, toPlaceSize, placingCardIndex, combatSystem.playerHand[placingCardIndex]);
            playerMarkers.Add(playerMarker);
            */

            slotNr = abi.StartTime;
            int toPlaceSize = abi.Abilitie.CastTime;

            GameObject enemyMarker = Instantiate(enemyMarkerPrefab, enemyMarkerSpace);

            RectTransform trans = enemyMarker.GetComponent<RectTransform>();
            trans.anchoredPosition = new Vector2(15 + 50 + 150 * slotNr, -40);
            trans.sizeDelta = new Vector2((150 * toPlaceSize) - 15, 45);
            // enemyMarker.GetComponent<EnemyMarker> // we dont show the player which cards are played!
            enemyMarkers.Add(enemyMarker);

        }
        return marker;
    }

    //w�hle karte x aus
    public void OpenCard(int cardIndex)
    {
        placingCardIndex = cardIndex;
        Abilitie abilitie = combatSystem.playerHand[cardIndex];
        cardInfo.SetActive(true);
        cardInfo.GetComponent<DisplayCard>().ShowCard(abilitie);
        slotHighlight.SetActive(true);
        placing = true;
        targetTimeSlot = 0;
        toPlaceSize = abilitie.CastTime;
        slotHighlight.GetComponent<RectTransform>().sizeDelta = new Vector2(150 * toPlaceSize, 100);
    }

    public void CloseCard()
    {
        cardInfo.SetActive(false);
        slotHighlight.SetActive(false);
        placing = false;
        targetTimeSlot = 0;
    }

    public void HoverSlot(int slotNr)
    {
        targetTimeSlot = slotNr;
    }

    //TODO Clickslot OnAddCardToTimeline
    public void ClickSlot(int slotNr)
    {
        if (placing)
        {
            if (CanPlaceCard())
            {
                targetTimeSlot = slotNr;
                cardInfo.SetActive(false);
                slotHighlight.SetActive(false);
                GameObject playerMarker = Instantiate(playerMarkerPrefab, payerMarkerSpace);
                playerMarker.GetComponent<RectTransform>().anchoredPosition = new Vector2(50 + 150 * slotNr, 30);
                playerMarker.GetComponent<RectTransform>().sizeDelta = new Vector2(150 * toPlaceSize, 90);
                playerMarker.GetComponent<PlayerMarker>().Initialize(targetTimeSlot, toPlaceSize, placingCardIndex, combatSystem.playerHand[placingCardIndex]);
                playerMarkers.Add(playerMarker);
                
                combatSystem.OnAddCardToTimeline(placingCardIndex, targetTimeSlot);
                handCards[placingCardIndex].SetActive(false);

                placing = false;
                targetTimeSlot = 0;

            }
        } else
        {
            GameObject playerMarker = GetMArkerAtSlot(slotNr);
            if (playerMarker != null)
                RemoveMarker(playerMarker);
                    
        }
    }

    private GameObject GetMArkerAtSlot(int slotNr)
    {
        foreach (GameObject playerMarker in playerMarkers)
        {
            if (slotNr == playerMarker.GetComponent<PlayerMarker>().slot)
            {
                return playerMarker;
            }
        }
        return null;
    }

    //TODO entweder in Timeline aufnehmen oder durch undo ersetzten
    public void RemoveMarker(GameObject marker) {
        if (combatSystem.DeleteFromTimeline(marker.GetComponent<PlayerMarker>().slot))
        {
            playerMarkers.Remove(marker);
            handCards[marker.GetComponent<PlayerMarker>().cardIndex].SetActive(true);
            Destroy(marker);
        }
    }

    public void UndoLastAction()
    {
        int removed_slot = combatSystem.OnUndo();
        if (removed_slot != -1)
        {
            GameObject marker = GetMArkerAtSlot(removed_slot);
            if (marker != null)
            {
                playerMarkers.Remove(marker);
                handCards[marker.GetComponent<PlayerMarker>().cardIndex].SetActive(true);
                Destroy(marker);
            }
        }
    }


    public void OnPointerClick(PointerEventData eventData)
    {
        CloseCard();
    }
}
