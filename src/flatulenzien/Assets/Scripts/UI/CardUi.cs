using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardUi : MonoBehaviour
{
    public Abilitie abilitie;
    public int cardIndex;
    private BattleUI battleUI;
    public Image Image;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Initialize(BattleUI battleUI,Abilitie abilitie, int cardIndex)
    {
        this.battleUI = battleUI;
        this.abilitie = abilitie;
        this.cardIndex = cardIndex;
        Image.sprite = abilitie.Artwork;
    }

    public void OpenCard()
    {
        battleUI.OpenCard(cardIndex);
    }
}
