using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardUi2 : MonoBehaviour
{
    public Abilitie abilitie;
    public int cardIndex;
    private CenterController centerController;
    public Image Image;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Initialize(CenterController centerController, Abilitie abilitie, int cardIndex)
    {
        this.centerController = centerController;
        this.abilitie = abilitie;
        this.cardIndex = cardIndex;
        Debug.Log(abilitie, abilitie.Artwork);
        if(abilitie.Artwork)
            Image.sprite = abilitie.Artwork;
    }

    public void OpenCard()
    {
        centerController.OpenCard(gameObject);
    }
}
