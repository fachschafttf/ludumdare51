using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

// ReSharper disable once UnusedMember.Global
// ReSharper disable once CheckNamespace
public class MainMenu : MonoBehaviour
{
    public Button PlayButton;
    // ReSharper disable once UnusedMember.Global

    void Start() {
        PlayButton.Select();
    }
    public void PlayGame(int gameMode)
    {
        Debug.Log("Play Level in Gamemode " + gameMode);
        SceneManager.LoadScene("MainLevel");
    }

    // ReSharper disable once UnusedMember.Global
    public void QuitGame()
    {
        Debug.Log("QuitGame");
        Application.Quit();
    }
}
