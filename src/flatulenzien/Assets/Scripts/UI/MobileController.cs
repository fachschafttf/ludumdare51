using UnityEngine;
using System.Runtime.InteropServices;

public class MobileController : MonoBehaviour
{
    [DllImport("__Internal")]
    private static extern bool MobileBrowserDetect();

    public static MobileController Instance;

    public GameObject controlsObject;

    public bool useMobileControls = false;
    public bool autodetectMobileControls = true;

    void Start() {
        useMobileControls = false;
        autodetectMobileControls = true;
        detectMobileControls();
    }

    void Awake() {
        DontDestroyOnLoad(gameObject);
        // Keep existing controller from different scene.
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }
    }

    public void detectMobileControls() {
        // detect mobile browsers here
        if (autodetectMobileControls) {
            if (Application.platform == RuntimePlatform.WebGLPlayer) {
                useMobileControls = MobileBrowserDetect();
            }
            else {
                useMobileControls = false;
            }
        }
    }

    public void disableControls() {
        controlsObject.SetActive(false);
    }

    public void enableControls() {
        if (useMobileControls) {
            controlsObject.SetActive(true);
        }
    }

    public void setMobileControls(bool val) {
        autodetectMobileControls = false;
        useMobileControls = val;
    }
}
