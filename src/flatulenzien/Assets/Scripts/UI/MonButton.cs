using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

public class MonButton : MonoBehaviour
{
    public int indx = 0;
    public CenterController canvas;
    public Monster mon = null;

    void Start()
    {
        canvas = GameObject.Find("Center").GetComponent<CenterController>();
    }

    public void Initialize(Monster mon)
    {
        this.mon = mon;
        transform.GetChild(0).GetComponent<Image>().sprite = mon.Artwork;
        transform.GetChild(0).GetComponent<Image>().color = new Color(255, 255, 225, 100);
        transform.GetChild(1).GetComponent<TMP_Text>().text = mon.MonsterName;
    }

    public void OnClick()
    {
        canvas.OnClickMon(gameObject);
    }
}
