using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

// ReSharper disable once CheckNamespace
public class Overlay : MonoBehaviour, MainInput.IIngameMenuActions
{
    private MainInput _controls;
    public GameObject Transparent;
    public GameObject PauseOverlay;
    public GameObject SettingsMenu;
    public GameObject GameOverOverlay;
    public GameObject SubmissionOverlay;
    public Button ResumeButton;
    public Button SubmitButton;

    private bool _gameOverDebug = false;

    public void OnPauseToggle(InputAction.CallbackContext context)
    {
        if (!context.performed)
        {
            return;
        }

        TogglePause();
    }

    void Update() {
        if (GameOverOverlay.activeSelf ||SubmissionOverlay.activeSelf)
        {
            
        }

    }



    public void TogglePause()
    {
        if (_gameOverDebug) {
            GameOver();
            return;
        }
        if (GameManager.Instance.Paused)
        {
            DisablePause();
        }
        else
        {
            EnablePause();
        }
    }

    public void GameOver()
    {
        Transparent.SetActive(true);
        GameOverOverlay.SetActive(true);
        SubmitButton.Select();
        GameManager.Instance.Paused = true;
    }


    public void Replay()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void EnablePause()
    {
        GameManager.Instance.Paused = true;
        Transparent.SetActive(true);
        PauseOverlay.SetActive(true);
        ResumeButton.Select();
    }

    public void DisablePause()
    {
        GameManager.Instance.Paused = false;
        Transparent.SetActive(false);
        PauseOverlay.SetActive(false);
        SettingsMenu.SetActive(false);
    }

    // ReSharper disable once UnusedMember.Global
    public void LoadMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }


    // Needed for input system
    // ReSharper disable once UnusedMember.Local
    private void OnEnable()
    {
        if (_controls == null)
        {
            _controls = new MainInput();
            _controls.IngameMenu.SetCallbacks(this);
        }
        _controls.Enable();
    }

    // Needed for input system
    // ReSharper disable once UnusedMember.Local
    private void OnDisable()
    {
        _controls.Disable();
    }
}
