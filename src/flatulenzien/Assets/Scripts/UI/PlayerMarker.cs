using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PlayerMarker : MonoBehaviour
{
    private BattleUI battleUI;
    public int slot;
    public int size;
    public int cardIndex;
    public Abilitie abilitie;
    public Image Image;

    // Start is called before the first frame update
    void Start()
    {
        battleUI = GetComponentInParent<BattleUI>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Initialize(int slot, int size, int cardIndex, Abilitie abilitie)
    {
        this.slot = slot;
        this.size = size;
        this.cardIndex = cardIndex;
        Image.sprite = abilitie.Artwork;
    }
}
