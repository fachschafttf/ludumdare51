using UnityEngine;
using UnityEngine.UI;

// ReSharper disable once CheckNamespace
public class SettingsSlider : MonoBehaviour
{
    
    public Slider MusicSlider;
    public Slider SfxSlider;

    public Sprite positiveSprite;

    public Sprite negativeSprite;

    // ReSharper disable once UnusedMember.Local
    void Start()
    {
        AdjustSlidersToCurrentLevel();
    }

    public void AdjustSlidersToCurrentLevel()
    {
        MusicSlider.value = AudioController.Instance.MusicVolume;
        SfxSlider.value = AudioController.Instance.SfxVolume;
    }

    // ReSharper disable once UnusedMember.Global
    public void SetMusicVolume(float val)
    {
        AudioController.Instance.SetMusicVolume((int)val);
    }

    // ReSharper disable once UnusedMember.Global
    public void SetSfxVolume(float val)
    {
        AudioController.Instance.SetSfxVolume((int)val);
    }

    public void SetMobilControls(bool val) {
        bool useMobileControls = !GameObject.Find("MobileController").GetComponent<MobileController>().useMobileControls;
        GameObject.Find("MobileController").GetComponent<MobileController>().setMobileControls(useMobileControls);
        if (useMobileControls) {
            GameObject.Find("TouchInputBoolean").GetComponentInChildren<Image>().sprite = positiveSprite;
        }
        else {
            GameObject.Find("TouchInputBoolean").GetComponentInChildren<Image>().sprite = negativeSprite;
        }
    }
}
