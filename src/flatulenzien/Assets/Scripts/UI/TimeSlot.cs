using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TimeSlot : MonoBehaviour, IPointerEnterHandler, IPointerClickHandler
{
    public int mySlotNr = 0;
    private BattleUI battleUI;

    public void OnPointerClick(PointerEventData eventData)
    {
        battleUI.ClickSlot(mySlotNr);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        battleUI.HoverSlot(mySlotNr);
    }

    // Start is called before the first frame update
    void Start()
    {
        battleUI = GetComponentInParent<BattleUI>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
