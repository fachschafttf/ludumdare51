using UnityEngine;

[System.Serializable]
// ReSharper disable once CheckNamespace
public class Sound
{
    [HideInInspector]
    public AudioSource Source;
    public AudioClip Clip;

    [Range(0, 1)]
    public float Volume = 1;

    public bool Loop;

    public void Play(float masterVolume)
    {
        if (Source != null)
        {
            Source.volume = Volume * masterVolume;
            Source.Play();
        }
    }

    public void PlayOneShot(float masterVolume)
    {
        if (Source != null)
        {
            Source.volume = Volume * masterVolume;
            Source.PlayOneShot(Clip);
        }
    }

    public void SetMasterVolume(float masterVolume)
    {
        Source.volume = Volume * masterVolume;
    }

    public void Stop()
    {
        Source.Stop();
    }

    public bool IsPlaying() {
        return Source.isPlaying;
    }

}
