using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class WorldManager : MonoBehaviour
{
    [SerializeField]
    private Tilemap groundTilemap;
    [SerializeField]
    private Tilemap collisionTilemap;

    private List<GameObject> interactableObjects = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        for(int i = 0; i < transform.Find("InteractableObjects").transform.childCount; i++)
        {
            interactableObjects.Add(transform.Find("InteractableObjects").GetChild(i).transform.gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateWorld()
    {
        interactableObjects.Clear();
        for (int i = 0; i < transform.Find("InteractableObjects").transform.childCount; i++)
        {
            interactableObjects.Add(transform.Find("InteractableObjects").GetChild(i).transform.gameObject);
        }
    }

    public GameObject GetInteractableObjectAt(Vector3 worldPos)
    {
        
        Vector3Int targetGridPosition = groundTilemap.WorldToCell(worldPos);
        foreach (GameObject interactableObject in interactableObjects)
        {
            if (interactableObject.active == false)
                continue;
            Vector3Int objPos = groundTilemap.WorldToCell(interactableObject.transform.position);
            if (Vector3Int.Distance(targetGridPosition, objPos) == 0)
            {
                return interactableObject;
            }
        }
        return null;
    }

    public bool CanMoveTo(Vector3 targetPos)
    {
        Vector3Int gridPosition = groundTilemap.WorldToCell(targetPos);
        if (!groundTilemap.HasTile(gridPosition) || collisionTilemap.HasTile(gridPosition))
            return false;
        if (GetInteractableObjectAt(targetPos) != null)
            return false;
        return true;
    }

    public Vector3 GetTileMiddle(Vector3 worldPos)
    {
        Vector3Int gridPosition = groundTilemap.WorldToCell(worldPos);
        return groundTilemap.GetCellCenterWorld(gridPosition);
    }

}
